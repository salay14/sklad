package cz.uhk.ppro.trash;

import cz.uhk.ppro.SpringConfig;
import cz.uhk.ppro.dao.AddressDao;
import cz.uhk.ppro.dao.CustomerDao;
import cz.uhk.ppro.dao.KindOfProductDao;
import cz.uhk.ppro.dao.OrderDao;
import cz.uhk.ppro.domain.Address;
import cz.uhk.ppro.domain.KindOfProduct;
import cz.uhk.ppro.domain.KindOfProductConverter;
import cz.uhk.ppro.entity.*;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfig.class})
public class AddressTest {

    @Autowired
    AddressDao addressDao;

    @Autowired
    KindOfProductDao kindOfProductDao;


    @Autowired
    CustomerDao customerDao;

    @Autowired
    OrderDao orderDao;

    //    @Autowired
    SessionFactory sessionFactory;

    @Ignore
    @Test
    public void entityTest(){
        EmployeeEntity employee = new EmployeeEntity();
        employee.setName("Karel");
        employee.setSurname("Novak");
        employee.setEmail("karel.novak@email.com");
        employee.setPhoneNumber("+420798123456");
        AddressEntity address = new AddressEntity();
        address.setMesto("Dolni Bousov");
        address.setPsc("29404");
        address.setUlice("Zahradky 318");
        employee.setAddress(address);

        CustomerEntity customer = new CustomerEntity();
        customer.setName("Franta");
        customer.setSurname("Salay");
        customer.setPhoneNumber("+1587789456");
        customer.setEmail("test@test.cz");
        AddressEntity address1 = new AddressEntity();
        address1.setMesto("Dolni Bousov");
        address1.setPsc("29404");
        address1.setUlice("Zahradky 318");
        customer.setAddress(address1);



//        employee.setOrders(orders);
//        customer.setOrders(orders);

        Integer idCustomer = (Integer) sessionFactory.getCurrentSession().save(customer);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        System.out.println("C: " +idCustomer);
        Integer idEmployee = (Integer) sessionFactory.getCurrentSession().save(employee);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        System.out.println("E: " +idEmployee);

        customer = sessionFactory.getCurrentSession().get(CustomerEntity.class, 24);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        employee = sessionFactory.getCurrentSession().get(EmployeeEntity.class, 34);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        System.out.println(customer.toString());
        System.out.println(employee.toString());

        sessionFactory.getCurrentSession().createSQLQuery("SET FOREIGN_KEY_CHECKS=0").executeUpdate();

        List<OrderEntity> orders = new ArrayList<>();
        OrderEntity order = new OrderEntity();
        order.setOrderingDate(new Date());
        order.setEmployee(employee);
        order.setCostumer(customer);
        orders.add(order);

        System.out.println(order.toString());
        sessionFactory.getCurrentSession().save(order);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();

        sessionFactory.getCurrentSession().createSQLQuery("SET FOREIGN_KEY_CHECKS=1").executeUpdate();

    }

    @Ignore
    @Test
    public void persist(){
        Address address = new Address();
        address.setCity("Dolni Bousov");
        address.setPostCode("29404");
        address.setStreet("Zahradky 318");

        Integer id = (Integer) addressDao.persist(address);

        Address address1 = addressDao.findById(id);

        Assert.assertEquals(address.getCity(), address1.getCity());

        addressDao.remove(address1);
     }

    @Ignore
    @Test
    public void kindOfProduct(){
        KindOfProduct kindOfProduct = new KindOfProduct();
        kindOfProduct.setUse("zahrada");

        Integer id = (Integer) kindOfProductDao.persist(kindOfProduct);

        KindOfProduct kindOfProduct1 = new KindOfProduct();

        kindOfProduct1 = kindOfProductDao.findById(id);

        Assert.assertEquals("zahrada", kindOfProduct1.getUse());

     }
}
