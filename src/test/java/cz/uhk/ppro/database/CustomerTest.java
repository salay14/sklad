/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package cz.uhk.ppro.database;

import cz.uhk.ppro.dao.CustomerDao;
import cz.uhk.ppro.domain.Address;
import cz.uhk.ppro.domain.Customer;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

public class CustomerTest extends SkladBaseTest {

    @Autowired
    private CustomerDao customerDao;

    @Ignore
    @Test
    @Override
    public void persist() {
        Customer customer = new Customer();
        customer.setFirstName("Karel");
        customer.setSurname("Novak");
        customer.setEmail("karel.novak@email.com");
        customer.setPhoneNumber("+420798123456");
        Address address = new Address();
        address.setCity("Dolni Bousov");
        address.setStreet("Zahradky 318");
        address.setPostCode("29404");
        customer.setAddress(address);

        Customer customerSaved = (Customer) customerDao.persist(customer);
        Customer c = customerDao.findById(customerSaved.getCustomerId());

        assertEquals("Karel",c.getFirstName());
        assertEquals("Novak",c.getSurname());
        assertEquals("karel.novak@email.com",c.getEmail());
        assertEquals("Dolni Bousov",c.getAddress().getCity());
        assertEquals("Zahradky 318",c.getAddress().getStreet());
        assertEquals("29404",c.getAddress().getPostCode());
        assertEquals("+420798123456",c.getPhoneNumber());
    }
}
