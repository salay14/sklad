/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package cz.uhk.ppro.database;

import cz.uhk.ppro.dao.*;
import cz.uhk.ppro.domain.*;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class OrderTest extends SkladBaseTest {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private RawMaterialDao rawMaterialDao;

    @Autowired
    private KindOfProductDao kindOfProductDao;

    @Ignore
    @Test
    @Override
    public void persist() {

        Customer customer = new Customer();
        customer.setFirstName("Karel");
        customer.setSurname("Novak");
        customer.setEmail("karel.novak@email.com");
        customer.setPhoneNumber("+420798123456");
        Address address = new Address();
        address.setCity("Dolni Bousov");
        address.setStreet("Zahradky 318");
        address.setPostCode("29404");
        customer.setAddress(address);

        Customer customerSaved = (Customer) customerDao.persist(customer);
        Customer c = customerDao.findById(customerSaved.getCustomerId());

        Employee employee = new Employee();
        employee.setFirstName("Frantisek");
        employee.setSurname("Novotny");
        Address eAddress = new Address();
        eAddress.setCity("Brno");
        eAddress.setStreet("15. kvetna");
        eAddress.setPostCode("15402");
        employee.setAddress(eAddress);
        employee.setPhoneNumber("+420739455124");
        employee.setEmail("franta.novotny@sdas.cz");

        Employee employeeSaved = (Employee) employeeDao.persist(employee);
        Employee e = employeeDao.findById(employeeSaved.getEmployeeId());

        /** CREATING PRODUCT WITH 2 RAW MATERIALS **/
        RawMaterial rawMaterial = new RawMaterial();
        rawMaterial.setName("prkno");
        rawMaterial.setWeight(50);
        rawMaterial.setAmount(10);
        rawMaterial.setMaterial("dub");

        RawMaterial id = (RawMaterial) rawMaterialDao.persist(rawMaterial);
        RawMaterial rawMaterialFound = rawMaterialDao.findById(id.getRawMaterialId());

        RawMaterial rawMaterial2 = new RawMaterial();
        rawMaterial2.setName("hřebíky");
        rawMaterial2.setWeight(1);
        rawMaterial2.setAmount(100);
        rawMaterial2.setMaterial("kov");

        RawMaterial idRM2 = (RawMaterial) rawMaterialDao.persist(rawMaterial2);
        RawMaterial rawMaterialFound2 = rawMaterialDao.findById(idRM2.getRawMaterialId());

        Product product = new Product();
        product.setName("Postel");
        product.setSerialNumber(1205080);
        product.setWidth(120);
        product.setHeight(50);
        product.setDepth(80);

        KindOfProduct kindOfProduct = new KindOfProduct();
        kindOfProduct.setUse("Loznice");
        KindOfProduct idKindProduct = (KindOfProduct) kindOfProductDao.persist(kindOfProduct);
        kindOfProduct = kindOfProductDao.findById(idKindProduct.getKindOfProductId());
        product.setKindOfProduct(kindOfProduct);

        List<ProductMaterialWrap> productMaterialWraps = new ArrayList<>();

        ProductMaterialWrap wrap = new ProductMaterialWrap();
        wrap.setRawMaterial(rawMaterialFound);
        wrap.setAmount(4);
        productMaterialWraps.add(wrap);

        ProductMaterialWrap wrap2 = new ProductMaterialWrap();
        wrap2.setRawMaterial(rawMaterialFound2);
        wrap2.setAmount(50);
        productMaterialWraps.add(wrap2);

        product.setMaterialProducts(productMaterialWraps);

        Product idProduct = (Product) productDao.persist(product);
        Product productFound = productDao.findById(idProduct.getProductId());

        /** CREATING ORDER**/

        Order order = new Order();
        order.setOrderingDate(new Date());

        order.setCustomer(c);
        order.setEmployee(e);

        List<ProductOrderWrap> productOrderWraps = new ArrayList<>();
        ProductOrderWrap productOrder = new ProductOrderWrap();

        productOrder.setProduct(productFound);
        productOrder.setColor("Brown");
        productOrder.setAmount(2);

        productOrderWraps.add(productOrder);
        order.setProductOrder(productOrderWraps);

        System.out.println("====SAVE====");
        Order orderSaved = (Order) orderDao.persist(order);
        Order orderFound = orderDao.findById(orderSaved.getOrderId());

        assertEquals(order.getCustomer().getFirstName(), orderFound.getCustomer().getFirstName());
        System.out.println(orderFound.getOrderId());

//        assertEquals(order,orderSaved);
//        assertEquals(orderSaved,orderFound);


        System.out.println("====UPDATE====");
        orderFound.setDeliveryDate(new Date());
        orderFound.getCustomer().setFirstName("Marenka");
        Order orderEdited = orderDao.merge(orderFound);

        System.out.println("====REMOVE====");
        orderDao.remove(orderEdited);

    }

    @Ignore
    @Test
    public void remove(){
        System.out.println("====REMOVE====");
        orderDao.removeById(14);
    }

}
