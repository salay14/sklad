package cz.uhk.ppro.database;

import cz.uhk.ppro.dao.EmployeeDao;
import cz.uhk.ppro.domain.*;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;


public class EmployeeTest extends SkladBaseTest {

    @Autowired
    EmployeeDao employeeDao;

    @Ignore
    @Test
    @Override
    public void persist() {
        Employee employee = new Employee();
        employee.setFirstName("Frantisek");
        employee.setSurname("Novotny");
        Address address = new Address();
        address.setCity("Brno");
        address.setStreet("15. kvetna");
        address.setPostCode("15402");
        employee.setAddress(address);
        employee.setPhoneNumber("+420739455124");
        employee.setEmail("franta.novotny@sdas.cz");

        Employee employeeSaved = (Employee) employeeDao.persist(employee);
        Employee e = employeeDao.findById(employeeSaved.getEmployeeId());
        employeeDao.remove(e);
        assertEquals("Frantisek",e.getFirstName());
        assertEquals("Novotny",e.getSurname());
        assertEquals("Brno",e.getAddress().getCity());
        assertEquals("15. kvetna",e.getAddress().getStreet());
        assertEquals("15402",e.getAddress().getPostCode());
        assertEquals("+420739455124",e.getPhoneNumber());
        assertEquals("franta.novotny@sdas.cz",e.getEmail());
    }

    @Test
    public void remove(){
        Employee employee = new Employee();
        employee.setFirstName("Frantisek");
        employee.setSurname("Novotny");
        Address address = new Address();
        address.setCity("Brno");
        address.setStreet("15. kvetna");
        address.setPostCode("15402");
        employee.setAddress(address);
        employee.setPhoneNumber("+420739455124");
        employee.setEmail("franta.novotny@sdas.cz");

        Employee employeeSaved = (Employee) employeeDao.persist(employee);
        Employee e = employeeDao.findById(employeeSaved.getEmployeeId());
        e.setFirstName("Jan");
        Employee employeeEdited = employeeDao.merge(e);
        employeeDao.remove(employeeEdited);

    }

}
