package cz.uhk.ppro.database;

import cz.uhk.ppro.dao.RawMaterialDao;
import cz.uhk.ppro.domain.RawMaterial;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class RawMaterialTest extends SkladBaseTest{

    @Autowired
    RawMaterialDao rawMaterialDao;

    @Test
    @Ignore
    public void listAll(){
        List<RawMaterial> rawMaterials = rawMaterialDao.findAll();

        Assert.assertNotNull(rawMaterials);
    }

    @Test
    @Ignore
    public void persist() {

        RawMaterial rawMaterial = new RawMaterial();
        rawMaterial.setName("prkno");
        rawMaterial.setWeight(50);
        rawMaterial.setAmount(10);
        rawMaterial.setMaterial("dub");

        RawMaterial id = (RawMaterial) rawMaterialDao.persist(rawMaterial);

        RawMaterial rawMaterial1 = rawMaterialDao.findById(id.getRawMaterialId());

      //  Assert.assertEquals(rawMaterial.getName(), rawMaterial1.getName());
        System.out.println("====UPDATE====");
        rawMaterial1.setAmount(44);
        rawMaterialDao.merge(rawMaterial1);
    }

}
