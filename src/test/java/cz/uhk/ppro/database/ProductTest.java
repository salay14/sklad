package cz.uhk.ppro.database;

import cz.uhk.ppro.dao.KindOfProductDao;
import cz.uhk.ppro.dao.ProductDao;
import cz.uhk.ppro.dao.RawMaterialDao;
import cz.uhk.ppro.domain.KindOfProduct;
import cz.uhk.ppro.domain.Product;
import cz.uhk.ppro.domain.ProductMaterialWrap;
import cz.uhk.ppro.domain.RawMaterial;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class ProductTest extends SkladBaseTest {

    @Autowired
    private RawMaterialDao rawMaterialDao;

    @Autowired
    KindOfProductDao kindOfProductDao;

    @Autowired
    ProductDao productDao;

    @Ignore
    @Test
    @Override
    public void persist() {

        RawMaterial rawMaterial = new RawMaterial();
        rawMaterial.setName("prkno");
        rawMaterial.setWeight(50);
        rawMaterial.setAmount(10);
        rawMaterial.setMaterial("dub");

        RawMaterial id = (RawMaterial) rawMaterialDao.persist(rawMaterial);
        RawMaterial rawMaterialFound = rawMaterialDao.findById(id.getRawMaterialId());

        RawMaterial rawMaterial2 = new RawMaterial();
        rawMaterial2.setName("hřebíky");
        rawMaterial2.setWeight(1);
        rawMaterial2.setAmount(100);
        rawMaterial2.setMaterial("kov");

        RawMaterial idRM2 = (RawMaterial) rawMaterialDao.persist(rawMaterial2);
        RawMaterial rawMaterialFound2 = rawMaterialDao.findById(idRM2.getRawMaterialId());


        Product product = new Product();
        product.setName("Postel");
        product.setSerialNumber(1205080);
        product.setWidth(120);
        product.setHeight(50);
        product.setDepth(80);

        KindOfProduct kindOfProduct = new KindOfProduct();
        kindOfProduct.setUse("Loznice");
        KindOfProduct idKindProduct = (KindOfProduct) kindOfProductDao.persist(kindOfProduct);
        kindOfProduct = kindOfProductDao.findById(idKindProduct.getKindOfProductId());
        product.setKindOfProduct(kindOfProduct);

        List<ProductMaterialWrap> productMaterialWraps = new ArrayList<>();

        ProductMaterialWrap wrap = new ProductMaterialWrap();
        wrap.setRawMaterial(rawMaterialFound);
        wrap.setAmount(4);
        productMaterialWraps.add(wrap);

        ProductMaterialWrap wrap2 = new ProductMaterialWrap();
        wrap2.setRawMaterial(rawMaterialFound2);
        wrap2.setAmount(50);
        productMaterialWraps.add(wrap2);

        product.setMaterialProducts(productMaterialWraps);

        System.out.println("====SAVE====");
        Product idProduct = (Product) productDao.persist(product);
        Product productFound = productDao.findById(idProduct.getProductId());
//
//        Assert.assertEquals(productFound.getName(), product.getName());
//        Assert.assertEquals(productFound.getKindOfProduct().getUse(), kindOfProduct.getUse());
//        Assert.assertEquals(productFound.getMaterialProducts().get(0).getRawMaterial().getName(), rawMaterialFound.getName());
//        Assert.assertEquals(productFound.getMaterialProducts().get(1).getRawMaterial().getName(), rawMaterialFound2.getName());
        System.out.println("====UPDATE====");
        productFound.setName("Nove jmeno");
        productFound.getMaterialProducts().get(0).setAmount(99);
        productFound.getMaterialProducts().get(0).setRawMaterial(rawMaterialDao.findById(10));
        productDao.merge(productFound);
    }
}
