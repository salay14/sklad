create table adresy
(
  adresyID int auto_increment
    primary key,
  mesto varchar(50) not null,
  PSC char(6) not null,
  ulice varchar(50) not null
)
;

create table druhy_surovin
(
  druhy_surovinID int auto_increment
    primary key,
  nazev varchar(50) not null
)
;

create table druhy_vyrobku
(
  druhy_vyrobkuID int auto_increment
    primary key,
  pouziti varchar(50) not null
)
;

create table suroviny
(
  surovinyID int auto_increment
    primary key,
  nazev varchar(50) not null,
  kusy int not null,
  material varchar(50) not null,
  vaha int not null,
  druhy_surovinID int not null
)
;

create index FK_druhy_surovin_suroviny
  on suroviny (druhy_surovinID)
;

create table suroviny_druhy_surovin
(
  kindOfRawMaterialEntityId_druhy_surovinID int not null,
  RawMaterialEntity_surovinyID int not null
)
;

create index suroviny_druhy_surovin_druhy_surovin_FK
  on suroviny_druhy_surovin (RawMaterialEntity_surovinyID)
;

create index suroviny_druhy_surovin_suroviny_FK
  on suroviny_druhy_surovin (kindOfRawMaterialEntityId_druhy_surovinID)
;

create table surovinyvyrobku
(
  surovinyID int not null,
  vyrobkyID int not null,
  kusy int not null,
  primary key (vyrobkyID, surovinyID),
  constraint FK_suroviny_surovinyvyrobku
  foreign key (surovinyID) references suroviny (surovinyID)
)
;

create index FK_suroviny_surovinyvyrobku
  on surovinyvyrobku (surovinyID)
;

create table ukoly
(
  taskId int not null
    primary key,
  nazev varchar(255) not null,
  description varchar(255) null,
  zakazkyID int not null,
  zamestnanciID int null
)
;

create index FK_zakazky_ukoly
  on ukoly (zakazkyID)
;

create index FK_zamestnanci_ukoly
  on ukoly (zamestnanciID)
;

create table vyrobky
(
  vyrobkyID int auto_increment
    primary key,
  seriove_cislo int not null,
  hloubka int not null,
  nazev varchar(50) not null,
  sirka int not null,
  vyska int not null,
  druhy_vyrobkuID int not null,
  constraint FK_druhy_vyrobku_vyrobky
  foreign key (druhy_vyrobkuID) references druhy_vyrobku (druhy_vyrobkuID)
)
;

create index FK_druhy_vyrobku_vyrobky
  on vyrobky (druhy_vyrobkuID)
;

alter table surovinyvyrobku
  add constraint FK_vyrobky_surovinyvyrobku
foreign key (vyrobkyID) references vyrobky (vyrobkyID)
;

create table vyrobkyzakazek
(
  vyrobkyZakazekID int auto_increment
    primary key,
  barva varchar(50) not null,
  mnozstvi int not null,
  vyrobkyID int not null,
  zakazkyID int not null,
  constraint FK_vyrobky
  foreign key (vyrobkyID) references vyrobky (vyrobkyID)
)
;

create index vyrobkyID
  on vyrobkyzakazek (vyrobkyID)
;

create index zakazkyID
  on vyrobkyzakazek (zakazkyID)
;

create table zakazky
(
  zakazkyID int auto_increment
    primary key,
  datum_dodani date null,
  datum_objednani date not null,
  zakazniciID int not null,
  zamestnanciID int not null
)
;

create index FK_zakaznici_zakazky
  on zakazky (zakazniciID)
;

create index FK_zamestnanci_zakazky
  on zakazky (zamestnanciID)
;

alter table ukoly
  add constraint FK_zakazky_ukoly
foreign key (zakazkyID) references zakazky (zakazkyID)
;

alter table vyrobkyzakazek
  add constraint FK_zakazky
foreign key (zakazkyID) references zakazky (zakazkyID)
;

create table zakaznici
(
  zakazniciID int auto_increment
    primary key,
  jmeno varchar(50) not null,
  prijmeni varchar(50) not null,
  email varchar(50) not null,
  telefon varchar(16) not null,
  adresyID int not null,
  constraint FK_adresy_zakaznici
  foreign key (adresyID) references adresy (adresyID)
)
;

create index FK_adresy_zakaznici
  on zakaznici (adresyID)
;

alter table zakazky
  add constraint FK_zakaznici_zakazky
foreign key (zakazniciID) references zakaznici (zakazniciID)
;

create table zamestnanci
(
  zamestnanciID int auto_increment
    primary key,
  jmeno varchar(50) not null,
  prijmeni varchar(50) not null,
  email varchar(50) not null,
  telefon varchar(16) not null,
  adresyID int not null,
  constraint FK_adresy_zamestnanci
  foreign key (adresyID) references adresy (adresyID)
)
;

create index adresyID
  on zamestnanci (adresyID)
;

alter table ukoly
  add constraint FK_zamestnanci_ukoly
foreign key (zamestnanciID) references zamestnanci (zamestnanciID)
;

alter table zakazky
  add constraint FK_zamestnanci_zakazky
foreign key (zamestnanciID) references zamestnanci (zamestnanciID)
;

