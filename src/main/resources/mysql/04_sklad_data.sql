﻿-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 30. led 2018, 04:41
-- Verze serveru: 10.1.29-MariaDB
-- Verze PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `sklad`
--

--
-- Vypisuji data pro tabulku `adresy`
--

INSERT INTO `adresy` (`adresyID`, `mesto`, `PSC`, `ulice`) VALUES
(43, 'Počátky', '394 58', 'Krátká 5'),
(44, 'Hradec Králové', '398 57', 'Benešova 189'),
(45, 'Hradec Králové', '398 57', 'Fazolová 1156'),
(46, 'Roudnice Nad Labem', '368 26', 'Hradecká 72'),
(47, 'Pardubice', '314 52', 'Vesnická 987'),
(48, 'Opatovice Nad Labem', '435 98', 'Nad Labem 2'),
(49, 'Hradec Králové', '364 15', 'Vítězné náměstí 152/8'),
(50, 'Stěžery', '326', '5'),
(51, 'Těchlovice', '302 65', 'U hradce 636'),
(52, 'Stěžery', '306 26', '123'),
(53, 'Stěžery', '306 26', '5'),
(54, 'Černovice', '456 11', 'Bytová 564'),
(55, 'Studená', '365 23', '444'),
(56, 'Studená', '639 26', 'Hlavní 58'),
(58, 'Aš', '123 45', '66'),
(59, 'Praha', '236 12', '65'),
(60, 'Hradec Králové', '398 56', 'Truhlářská 985/3'),
(61, 'Chomutov', '394 56', 'Sloní stezka 362'),
(62, 'Stranná', '365 21', 'Lesní 35'),
(63, 'Praha', '236 12', 'Královská 932'),
(64, 'Hradec Králové', '398 56', 'Náměstí 98'),
(65, 'Žirovnice', '394 68', 'Perky 69'),
(66, 'Polesí', '365 21', 'Pod pařezem 96'),
(67, 'Žirovnice', '394 68', 'Perky 69'),
(68, 'Žirovnice', '394 68', 'Sídliště 666'),
(69, 'Hradec Králové', '398 56', 'Náměstí 98');

--
-- Vypisuji data pro tabulku `druhy_vyrobku`
--

INSERT INTO `druhy_vyrobku` (`druhy_vyrobkuID`, `pouziti`) VALUES
(5, 'stůl'),
(6, 'židle'),
(7, 'skříň'),
(8, 'police'),
(9, 'postel'),
(10, 'dveře');

--
-- Vypisuji data pro tabulku `suroviny`
--

INSERT INTO `suroviny` (`surovinyID`, `nazev`, `kusy`, `material`, `vaha`) VALUES
(12, 'EGGER DT 16', 150, 'dřevotříska', 12),
(13, 'EGGER DT 32', 354, 'dřevotříska', 24),
(14, 'Kronospan OSB 4D 20', 15, 'OSB', 6),
(15, 'Kronospan OSB C/4 20', 3, 'OSB', 6),
(16, 'Smrkové prkno - hobl 18 x 200 x 5000', 154, 'smrk', 4),
(17, 'Dub PR H - 25 x 200 x 4000', 26, 'dub', 5),
(18, 'Smrk PA - 16 x 190 x 5000', 360, 'smrk', 4),
(19, 'Smrk PR S - 18 x 200 x 500', 79, 'smrk', 4),
(20, 'Třešeň PR H - 18 x 200 x 5000', 42, 'třešeň', 5),
(21, 'Sklo klasik 7', 14, 'sklo', 3),
(22, 'Sklo T kouř - 12', 5, 'sklo', 5),
(23, 'Sklo T klasik - 12', 6, 'sklo', 4),
(24, 'KovoZoid - PT nerez', 265, 'nerez', 1),
(25, 'KovoZoid - PT černé', 136, 'kov', 1),
(26, 'KovoZoid - zámek ST nerez', 958, 'nerez', 1),
(27, 'Hřebík O - 4,5 x 100', 126542, 'ocel', 1),
(28, 'Hřebík O - 3 x 7', 216314, 'ocel', 1),
(29, 'Vrut ZN plný - 2,5 x 16', 163233, 'zinek', 1),
(30, 'Vrut ZN půl - 2,5 x 16', 3221, 'zinek', 1),
(31, 'Vrut NER plný - 2,5 x 16', 56312, 'nerez', 1),
(32, 'Vrut NER půl - 2,5 x 16', 2316, 'nerez', 1);

--
-- Vypisuji data pro tabulku `surovinyvyrobku`
--

INSERT INTO `surovinyvyrobku` (`surovinyID`, `vyrobkyID`, `kusy`) VALUES
(18, 5, 3),
(22, 5, 1),
(32, 5, 25),
(12, 6, 1),
(29, 6, 25),
(12, 7, 1),
(13, 7, 2),
(23, 7, 1),
(24, 7, 4),
(26, 7, 2),
(31, 7, 75),
(17, 8, 9),
(29, 8, 65),
(12, 9, 2),
(31, 9, 15),
(12, 10, 2),
(31, 10, 26),
(20, 11, 2),
(29, 11, 15),
(18, 12, 2),
(19, 12, 8),
(13, 13, 3),
(30, 13, 50),
(13, 14, 1),
(27, 14, 50),
(13, 15, 5),
(31, 15, 60),
(19, 16, 3),
(23, 16, 1),
(29, 16, 22),
(14, 17, 2),
(24, 17, 2),
(26, 17, 1),
(20, 18, 4),
(24, 18, 2),
(26, 18, 1),
(15, 19, 2),
(22, 19, 1),
(24, 19, 2),
(26, 19, 1);

--
-- Vypisuji data pro tabulku `vyrobky`
--

INSERT INTO `vyrobky` (`vyrobkyID`, `seriove_cislo`, `hloubka`, `nazev`, `sirka`, `vyska`, `druhy_vyrobkuID`) VALUES
(5, 111, 1000, 'PC stůl SMART', 2000, 700, 5),
(6, 2, 500, 'Konferenční stůl ECONOMY', 1000, 700, 5),
(7, 3, 700, 'Skříň MAXI', 2000, 3000, 7),
(8, 4, 90, 'Office Dub 2018', 1800, 700, 5),
(9, 5, 300, 'Modern 2018 - závěsná', 1500, 300, 8),
(10, 6, 1000, 'Klasik MT17', 25, 35, 8),
(11, 7, 300, 'Hall třešeň - závěsná', 1000, 300, 8),
(12, 265, 1000, 'Kuchyň RETRO MINI', 900, 600, 5),
(13, 331, 2500, 'Dream Standard', 1100, 400, 9),
(14, 301, 2500, 'Dream Comfort', 1300, 400, 9),
(15, 306, 3000, 'Dream Maxi', 1600, 450, 9),
(16, 1112, 800, 'Kuchyň L 2018', 1500, 700, 5),
(17, 400, 32, 'Dekor wood', 700, 2200, 10),
(18, 401, 45, 'Dveře třešeň', 700, 2200, 10),
(19, 402, 45, 'Dveře Dekor dub/sklo', 700, 2200, 10);

--
-- Vypisuji data pro tabulku `vyrobkyzakazek`
--

INSERT INTO `vyrobkyzakazek` (`vyrobkyZakazekID`, `barva`, `mnozstvi`, `vyrobkyID`, `zakazkyID`) VALUES
(3, 'brown', 15, 5, 3),
(4, 'brown', 1, 8, 3),
(5, 'red', 10, 6, 3),
(6, 'brown', 1, 15, 4),
(7, 'pink', 11, 11, 4),
(8, 'pink', 10, 13, 4),
(9, 'brown', 2, 12, 5),
(10, 'red', 46, 18, 5),
(11, 'brown', 23, 19, 5),
(12, 'red', 46, 10, 5),
(13, 'green', 66, 5, 6),
(14, 'red', 1, 6, 7),
(15, 'red', 1, 19, 7),
(16, 'red', 1, 7, 7),
(17, 'red', 1, 16, 7);

--
-- Vypisuji data pro tabulku `zakazky`
--

INSERT INTO `zakazky` (`zakazkyID`, `datum_dodani`, `datum_objednani`, `zakazniciID`, `zamestnanciID`) VALUES
(2, '2018-01-26', '2018-01-30', 9, 20),
(3, '2018-01-19', '2018-01-30', 12, 13),
(4, '2018-01-24', '2018-01-30', 14, 11),
(5, '2018-01-03', '2018-01-30', 18, 15),
(6, '2018-01-19', '2018-01-30', 12, 13),
(7, '2018-01-10', '2018-01-30', 22, 21);

--
-- Vypisuji data pro tabulku `zakaznici`
--

INSERT INTO `zakaznici` (`zakazniciID`, `jmeno`, `prijmeni`, `email`, `telefon`, `adresyID`) VALUES
(9, 'Klára', 'Nejedlá', 'clarisNejedla@seznam.cz', '606 625 692', 56),
(11, 'Tomáš', 'Kozoroh', 'rohac.tom@seznam.cz', '603 652 451', 58),
(12, 'Petr', 'Komárek', 'petr.komarek@google.com', '656 584 213', 59),
(13, 'Štěpán', 'Přesnídávka', 'prenidavka.mladi@seznam.cz', '625 589 787', 60),
(14, 'Přemek', 'Podlaha', 'premek.podlaha@google.com', '789 745 213', 61),
(15, 'František', 'Záruba', 'petr.zaruba@seznam.cz', '789 587 458', 62),
(16, 'Otakar', 'Sobotka', 'otikSS@seznam.cz', '697 584 233', 63),
(17, 'Bořivoj', 'Zlámaný', 'zlamany.borek@google.com', '660 815 236', 64),
(18, 'Miroslav', 'Špalek', 'špalek.mir3@seznam.cz', '782 145 258', 65),
(19, 'Petra', 'Šulcová', 'petaSulda@seznam.cz', '777 145 233', 66),
(20, 'Miroslav', 'Špalek', 'spalek.mir3@seznam.cz', '782 145 258', 67),
(21, 'Petr', 'Kejval', 'Kejval.petr@google.com', '666 562 362', 68),
(22, 'Bořivoj', 'Zlámaný', 'zlamany.borek@google.com', '660 815 236', 69);

--
-- Vypisuji data pro tabulku `zamestnanci`
--

INSERT INTO `zamestnanci` (`zamestnanciID`, `jmeno`, `prijmeni`, `email`, `telefon`, `adresyID`) VALUES
(11, 'Klára', 'Smetáková', 'clara8@gmail.com', '606 656 879', 43),
(12, 'František', 'Dobrota', 'dobro@seznam.cz', '720 897 546', 44),
(13, 'Tomáš', 'Vorel', 'vorlis88@gmail.com', '666 551 846', 45),
(14, 'Petr', 'Muchomůrka', 'cerveny.petr@seznam.cz', '787 545 265', 46),
(15, 'Lenka', 'Ušatá', 'usataLenka@gmail.com', '756 895 111', 47),
(16, 'Martin', 'Nula', 'martas@seznam.cz', '721 546 235', 48),
(17, 'Osfald', 'Hrdina', 'asfalt@gmail.com', '736 968 142', 49),
(18, 'Helmut', 'Schwarzenberg', 'schwarz.seznam.cz', '601 121 125', 50),
(19, 'Břetislav', 'Beránek', 'berry@gmail.com', '603 303 002', 51),
(20, 'Alžběta', 'Moravová', 'žirafa3@seznam.cz', '754 895 136', 52),
(21, 'Helmut', 'Schwarzenberg', 'schwarz.seznam.cz', '601 121 125', 53),
(22, 'Petr', 'Hovorka', 'hovisPet@seznam.cz', '777 879 526', 54),
(23, 'Pavel', 'Svoboda', 'svobi@seznam.cz', '754 885 223', 55);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
