package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.Customer;
import cz.uhk.ppro.domain.CustomerConverter;
import cz.uhk.ppro.entity.CustomerEntity;
import cz.uhk.ppro.util.Converter;
import cz.uhk.ppro.util.dao.CRUDDaoBase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDaoImpl extends CRUDDaoBase<Customer, Integer, CustomerEntity, Integer> implements CustomerDao {

    public static final CustomerConverter CONVERTER = new CustomerConverter();

    @Autowired
    SessionFactory sessionFactory;

    @Override
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Class<CustomerEntity> getEntityClass() {
        return CustomerEntity.class;
    }

    @Override
    public String getEntityKeyColumnId() {
        return "zakazniciID";
    }

    @Override
    public Converter<CustomerEntity, Customer> getConverter() {
        return CONVERTER;
    }

    @Override
    protected Integer toPrimaryKey(Integer valueObjectKey) {
        return valueObjectKey;
    }
}
