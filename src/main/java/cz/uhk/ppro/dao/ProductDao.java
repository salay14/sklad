package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.Product;
import cz.uhk.ppro.util.dao.CRUDDao;

public interface ProductDao extends CRUDDao<Product, Integer>{
}
