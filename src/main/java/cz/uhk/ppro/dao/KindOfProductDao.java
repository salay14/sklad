package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.KindOfProduct;
import cz.uhk.ppro.util.dao.CRUDDao;


public interface KindOfProductDao extends CRUDDao<KindOfProduct, Integer> {

}
