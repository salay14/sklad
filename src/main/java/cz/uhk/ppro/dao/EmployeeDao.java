package cz.uhk.ppro.dao;


import cz.uhk.ppro.domain.Employee;
import cz.uhk.ppro.util.dao.CRUDDao;

public interface EmployeeDao extends CRUDDao<Employee, Integer> {
}
