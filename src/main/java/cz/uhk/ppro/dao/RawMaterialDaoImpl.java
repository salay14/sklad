package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.RawMaterial;
import cz.uhk.ppro.domain.RawMaterialConverter;
import cz.uhk.ppro.entity.RawMaterialEntity;
import cz.uhk.ppro.util.Converter;
import cz.uhk.ppro.util.dao.CRUDDaoBase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RawMaterialDaoImpl extends CRUDDaoBase<RawMaterial, Integer, RawMaterialEntity, Integer> implements RawMaterialDao{

    private static final RawMaterialConverter CONVERTER = new RawMaterialConverter();

    @Autowired
    SessionFactory sessionFactory;

    @Override
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Class<RawMaterialEntity> getEntityClass() {
        return RawMaterialEntity.class;
    }

    @Override
    public String getEntityKeyColumnId() {
        return "surovinyID";
    }

    @Override
    public Converter<RawMaterialEntity, RawMaterial> getConverter() {
        return CONVERTER;
    }

    @Override
    protected Integer toPrimaryKey(Integer valueObjectKey) {
        return valueObjectKey;
    }
}
