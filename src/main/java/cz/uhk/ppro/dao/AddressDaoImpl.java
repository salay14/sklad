package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.Address;
import cz.uhk.ppro.domain.AddressConverter;
import cz.uhk.ppro.entity.AddressEntity;
import cz.uhk.ppro.util.Converter;
import cz.uhk.ppro.util.dao.CRUDDaoBase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AddressDaoImpl extends CRUDDaoBase<Address, Integer, AddressEntity, Integer> implements AddressDao {

    private static final AddressConverter CONVERTER = new AddressConverter();

    @Autowired
    SessionFactory sessionFactory;

    @Override
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Class<AddressEntity> getEntityClass() {
        return AddressEntity.class;
    }

    @Override
    public String getEntityKeyColumnId() {
        return "adresyID";
    }

    @Override
    public Converter<AddressEntity, Address> getConverter() {
        return CONVERTER;
    }

    @Override
    protected Integer toPrimaryKey(Integer valueObjectKey) {
        return valueObjectKey;
    }

}
