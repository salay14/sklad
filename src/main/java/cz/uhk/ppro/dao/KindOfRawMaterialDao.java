package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.KindOfRawMaterial;
import cz.uhk.ppro.entity.KindOfRawMaterialEntity;
import cz.uhk.ppro.util.dao.CRUDDao;

@Deprecated
public interface KindOfRawMaterialDao extends CRUDDao<KindOfRawMaterial, Integer> {
}
