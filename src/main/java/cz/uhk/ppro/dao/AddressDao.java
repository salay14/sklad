package cz.uhk.ppro.dao;


import cz.uhk.ppro.domain.Address;
import cz.uhk.ppro.util.dao.CRUDDao;

public interface AddressDao extends CRUDDao<Address, Integer>{
}
