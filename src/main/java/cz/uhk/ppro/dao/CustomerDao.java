package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.Customer;
import cz.uhk.ppro.util.dao.CRUDDao;

public interface CustomerDao extends CRUDDao<Customer, Integer>{
}
