/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.Order;
import cz.uhk.ppro.domain.OrderConverter;
import cz.uhk.ppro.entity.OrderEntity;
import cz.uhk.ppro.util.Converter;
import cz.uhk.ppro.util.dao.CRUDDaoBase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderDaoImpl extends CRUDDaoBase<Order, Integer, OrderEntity, Integer> implements OrderDao {

    private static final OrderConverter CONVERTER = new OrderConverter();

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Class<OrderEntity> getEntityClass() {
        return OrderEntity.class;
    }

    @Override
    public String getEntityKeyColumnId() {
        return "zakazkyID";
    }

    @Override
    public Converter<OrderEntity, Order> getConverter() {
        return CONVERTER;
    }

    @Override
    protected Integer toPrimaryKey(Integer domainKey) {
        return domainKey;
    }
}
