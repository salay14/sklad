package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.Employee;
import cz.uhk.ppro.domain.EmployeeConverter;
import cz.uhk.ppro.entity.EmployeeEntity;
import cz.uhk.ppro.util.Converter;
import cz.uhk.ppro.util.dao.CRUDDaoBase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDaoImpl extends CRUDDaoBase<Employee, Integer, EmployeeEntity, Integer> implements EmployeeDao{

    public static final EmployeeConverter CONVERTER = new EmployeeConverter();

    @Autowired
    SessionFactory sessionFactory;

    @Override
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Class<EmployeeEntity> getEntityClass() {
        return EmployeeEntity.class;
    }

    @Override
    public String getEntityKeyColumnId() {
        return "zamestnanciID";
    }

    @Override
    public Converter<EmployeeEntity, Employee> getConverter() {
        return CONVERTER;
    }

    @Override
    protected Integer toPrimaryKey(Integer valueObjectKey) {
        return valueObjectKey;
    }
}
