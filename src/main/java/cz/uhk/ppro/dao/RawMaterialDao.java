package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.RawMaterial;
import cz.uhk.ppro.util.dao.CRUDDao;

public interface RawMaterialDao extends CRUDDao<RawMaterial, Integer>{
}
