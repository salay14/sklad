package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.KindOfProduct;
import cz.uhk.ppro.domain.KindOfProductConverter;
import cz.uhk.ppro.util.Converter;
import cz.uhk.ppro.util.dao.CRUDDaoBase;
import org.hibernate.Session;

import cz.uhk.ppro.HibernateConfig;
import cz.uhk.ppro.entity.KindOfProductEntity;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Repository
public class KindOfProductDaoImpl extends CRUDDaoBase<KindOfProduct, Integer, KindOfProductEntity, Integer> implements  KindOfProductDao{

	private static final KindOfProductConverter CONVERTER = new KindOfProductConverter();

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public Class<KindOfProductEntity> getEntityClass() {
		return KindOfProductEntity.class;
	}

	@Override
	public String getEntityKeyColumnId() {
		return "druhy_vyrobkuID";
	}

	@Override
	public Converter<KindOfProductEntity, KindOfProduct> getConverter() {
		return CONVERTER;
	}

	@Override
	protected Integer toPrimaryKey(Integer domainKey) {
		return domainKey;
	}


//
//	@Override
////	@Transactional
//	public Serializable add(KindOfProductEntity kindOfProductEntity) {
//		return getCurrentSession().save(kindOfProductEntity);
//	}
//
//	@Override
//	public Session getCurrentSession() {
//		return sessionFactory.getCurrentSession();
//	}

//	public KindOfProductEntity add(KindOfProductEntity entity){
//		/*Session session = HibernateConfig.createSessionFactory().getCurrentSession();
//		session.beginTransaction();
//		session.saveOrUpdate(entity);
//		session.getTransaction().commit();*/
//		return entity;
//	}

}
