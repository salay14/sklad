package cz.uhk.ppro.dao;

import cz.uhk.ppro.domain.KindOfRawMaterial;
import cz.uhk.ppro.domain.KindOfRawMaterialConverter;
import cz.uhk.ppro.entity.KindOfRawMaterialEntity;
import cz.uhk.ppro.util.Converter;
import cz.uhk.ppro.util.dao.CRUDDaoBase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class KindOfRawMaterialDaoImpl extends CRUDDaoBase<KindOfRawMaterial, Integer, KindOfRawMaterialEntity, Integer> implements KindOfRawMaterialDao {

    private static final KindOfRawMaterialConverter CONVERTER = new KindOfRawMaterialConverter();

    @Autowired
    private  SessionFactory sessionFactory;

    @Override
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Class<KindOfRawMaterialEntity> getEntityClass() {
        return KindOfRawMaterialEntity.class;
    }

    @Override
    public String getEntityKeyColumnId() {
        return "druhy_surovinID";
    }

    @Override
    public Converter<KindOfRawMaterialEntity, KindOfRawMaterial> getConverter() {
        return CONVERTER;
    }

    @Override
    protected Integer toPrimaryKey(Integer valueObjectKey) {
        return valueObjectKey;
    }
}
