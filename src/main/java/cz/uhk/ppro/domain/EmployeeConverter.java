package cz.uhk.ppro.domain;

import cz.uhk.ppro.entity.AddressEntity;
import cz.uhk.ppro.entity.EmployeeEntity;
import cz.uhk.ppro.util.Converter;

public class EmployeeConverter extends Converter<EmployeeEntity, Employee>{

    private static final AddressConverter CONVERTER = new AddressConverter();

    @Override
    public EmployeeEntity toEntity(Employee domain) {
        if (domain == null){
            return null;
        }

        EmployeeEntity entity = new EmployeeEntity();
        entity.setEmployeeId(domain.getEmployeeId());
        entity.setName(domain.getFirstName());
        entity.setSurname(domain.getSurname());
        entity.setEmail(domain.getEmail());
        entity.setPhoneNumber(domain.getPhoneNumber());

        if (domain.getAddress() != null){

        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setAdresyId(domain.getAddress().getAddressId());
        addressEntity.setMesto(domain.getAddress().getCity());
        addressEntity.setUlice(domain.getAddress().getStreet());
        addressEntity.setPsc(domain.getAddress().getPostCode());
//        addressEntity.setMesto(domain.getCity());
//        addressEntity.setPsc(domain.getPostCode());
//        addressEntity.setUlice(domain.getStreet());
        entity.setAddress(addressEntity);

        }

        return entity;
    }

    @Override
    public Employee toDTO(EmployeeEntity entity) {
        if (entity == null){
            return null;
        }

        Employee domain = new Employee();
        domain.setEmployeeId(entity.getEmployeeId());
        domain.setFirstName(entity.getName());
        domain.setPhoneNumber(entity.getPhoneNumber());
        domain.setSurname(entity.getSurname());
        domain.setEmail(entity.getEmail());
        if (entity.getAddress() != null){
            Address address = new Address();
            address.setAddressId(entity.getAddress().getAdresyId());
            address.setCity(entity.getAddress().getMesto());
            address.setStreet(entity.getAddress().getUlice());
            address.setPostCode(entity.getAddress().getPsc());
//            domain.setCity(entity.getAddress().getMesto());
//            domain.setPostCode(entity.getAddress().getPsc());
//            domain.setStreet(entity.getAddress().getUlice());
            domain.setAddress(address);
        }
        return domain;
    }
}
