package cz.uhk.ppro.domain;

import cz.uhk.ppro.entity.AddressEntity;
import cz.uhk.ppro.entity.CustomerEntity;
import cz.uhk.ppro.util.Converter;

public class CustomerConverter extends Converter<CustomerEntity, Customer> {

    private static final AddressConverter addressConverter = new AddressConverter();

    @Override
    public CustomerEntity toEntity(Customer domain) {
        if (domain == null){
            return null;
        }

        CustomerEntity entity = new CustomerEntity();
        entity.setZakazniciId(domain.getCustomerId());
        entity.setName(domain.getFirstName());
        entity.setSurname(domain.getSurname());
        entity.setEmail(domain.getEmail());
        entity.setPhoneNumber(domain.getPhoneNumber());

//        AddressEntity addressEntity = new AddressEntity();
//        addressEntity.setAdresyId(domain.getAddress().getAddressId());
//        addressEntity.setMesto(domain.getAddress().getCity());
//        addressEntity.setUlice(domain.getAddress().getStreet());
//        addressEntity.setPsc(domain.getAddress().getPostCode());
//        addressEntity.setMesto(domain.getCity());
//        addressEntity.setUlice(domain.getStreet());
//        addressEntity.setPsc(domain.getPostCode());
        if (domain.getAddress() != null){
            entity.setAddress(addressConverter.toEntity(domain.getAddress()));
        }
        return entity;
    }

    @Override
    public Customer toDTO(CustomerEntity entity) {
        if (entity == null) {
            return null;
        }

        Customer domain = new Customer();
        domain.setCustomerId(entity.getZakazniciId());
        domain.setFirstName(entity.getName());
        domain.setSurname(entity.getSurname());
        domain.setEmail(entity.getEmail());
        domain.setPhoneNumber(entity.getPhoneNumber());
        if (entity.getAddress() != null){
//            domain.setCity(entity.getAddress().getMesto());
//            domain.setStreet(entity.getAddress().getUlice());
//            domain.setPostCode(entity.getAddress().getPsc());
//            Address address = new Address();
//            address.setAddressId(entity.getAddress().getAdresyId());
//            address.setCity(entity.getAddress().getMesto());
//            address.setStreet(entity.getAddress().getUlice());
//            address.setPostCode(entity.getAddress().getPsc());
            domain.setAddress(addressConverter.toDTO(entity.getAddress()));
        }

        return domain;
    }
}
