/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package cz.uhk.ppro.domain;

import cz.uhk.ppro.entity.OrderEntity;
import cz.uhk.ppro.entity.ProductOrderEntity;
import cz.uhk.ppro.util.Converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OrderConverter extends Converter<OrderEntity, Order> {

    private static final CustomerConverter customerConverter = new CustomerConverter();
    private static final EmployeeConverter employeeConverter = new EmployeeConverter();

    private final ProductConverter productConverter = new ProductConverter();

    @Override
    public OrderEntity toEntity(Order domain) {
        if (domain == null){
            return null;
        }
        OrderEntity entity = new OrderEntity();
        entity.setOrderId(domain.getOrderId());
        entity.setDeliveryDate(domain.getDeliveryDate());
        entity.setOrderingDate(domain.getOrderingDate());

        entity.setCostumer(customerConverter.toEntity(domain.getCustomer()));
        entity.setEmployee(employeeConverter.toEntity(domain.getEmployee()));

        Set<ProductOrderEntity> productOrderEntities = new HashSet<>();
        for (ProductOrderWrap wrap:
             domain.getProductOrder()) {

            ProductOrderEntity productOrderEntity = new ProductOrderEntity();
            productOrderEntity.setProduct(productConverter.toEntity(wrap.getProduct()));
            productOrderEntity.setOrder(entity);
            productOrderEntity.setColor(wrap.getColor());
            productOrderEntity.setAmount(wrap.getAmount());
            productOrderEntities.add(productOrderEntity);
        }

        entity.setProductOrderEntities(productOrderEntities);
        return entity;
    }

    @Override
    public Order toDTO(OrderEntity entity) {
        if (entity == null){
            return null;
        }
        Order order = new Order();
        order.setOrderId(entity.getOrderId());
        order.setCustomer(customerConverter.toDTO(entity.getCostumer()));
        order.setEmployee(employeeConverter.toDTO(entity.getEmployee()));
        order.setDeliveryDate(entity.getDeliveryDate());
        order.setOrderingDate(entity.getOrderingDate());

        List<ProductOrderWrap> materialWrapList = new ArrayList<>();
        for (ProductOrderEntity productOrderEntity:
             entity.getProductOrderEntities()) {

            ProductOrderWrap wrap = new ProductOrderWrap();
            wrap.setProduct(productConverter.toDTO(productOrderEntity.getProduct()));
            wrap.setAmount(productOrderEntity.getAmount());
            wrap.setColor(productOrderEntity.getColor());
            materialWrapList.add(wrap);
        }

        order.setProductOrder(materialWrapList);
        return order;
    }
}
