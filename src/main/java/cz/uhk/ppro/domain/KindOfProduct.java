package cz.uhk.ppro.domain;

import java.io.Serializable;

import sun.security.x509.SerialNumber;

public class KindOfProduct implements Serializable{

    private Integer kindOfProductId;
    private String use;

    public KindOfProduct(){
        super();
    }

    public Integer getKindOfProductId() {
        return kindOfProductId;
    }

    public void setKindOfProductId(Integer kindOfProductId) {
        this.kindOfProductId = kindOfProductId;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }
    
    
 /*   @Override
    public String toString() {
        return use;
    }
*/
}
