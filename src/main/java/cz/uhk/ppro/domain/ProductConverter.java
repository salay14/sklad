/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package cz.uhk.ppro.domain;

import cz.uhk.ppro.entity.MaterialProductEntity;
import cz.uhk.ppro.entity.MaterialProductPk;
import cz.uhk.ppro.entity.ProductEntity;
import cz.uhk.ppro.util.Converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ProductConverter extends Converter<ProductEntity, Product> {

    private final KindOfProductConverter kindOfProductConverter = new KindOfProductConverter();
    private final RawMaterialConverter rawMaterialConverter = new RawMaterialConverter();

    @Override
    public ProductEntity toEntity(Product domain) {
        if (domain == null){
            return null;
        }
        ProductEntity entity = new ProductEntity();
        entity.setProductId(domain.getProductId());
        entity.setName(domain.getName());
        entity.setSerialNumber(domain.getSerialNumber());
        entity.setDepth(domain.getDepth());
        entity.setHeight(domain.getHeight());
        entity.setWidth(domain.getWidth());
        entity.setKindOfProduct(kindOfProductConverter.toEntity(domain.getKindOfProduct()));

        Set<MaterialProductEntity> materialProductEntitySet = new HashSet<>();

        //List<ProductMaterialWrap> materialWrapList = domain.getMaterialProducts();
        for (ProductMaterialWrap productMaterialWrap:
            domain.getMaterialProducts()) {
            MaterialProductEntity materialProductEntity = new MaterialProductEntity();
            MaterialProductPk pk = new MaterialProductPk();
            pk.setMaterial(rawMaterialConverter.toEntity(productMaterialWrap.getRawMaterial()));
            pk.setProduct(entity);
            materialProductEntity.setMaterialProductPk(pk);
            materialProductEntity.setAmount(productMaterialWrap.getAmount());
            materialProductEntitySet.add(materialProductEntity);
        }


        entity.setMaterialProducts(materialProductEntitySet);

        return entity;
    }

    @Override
    public Product toDTO(ProductEntity entity) {
        if (entity == null){
            return null;
        }
        Product domain = new Product();
        domain.setProductId(entity.getProductId());
        domain.setName(entity.getName());
        domain.setSerialNumber(entity.getSerialNumber());
        domain.setDepth(entity.getDepth());
        domain.setHeight(entity.getHeight());
        domain.setWidth(entity.getWidth());
        domain.setKindOfProduct(kindOfProductConverter.toDTO(entity.getKindOfProduct()));

        List<ProductMaterialWrap> materialWrapList = new ArrayList<>();

        for (MaterialProductEntity materProdEntity:
            entity.getMaterialProducts()) {
            ProductMaterialWrap wrap = new ProductMaterialWrap();
            wrap.setRawMaterial(rawMaterialConverter.toDTO(materProdEntity.getRawMaterial()));
            wrap.setAmount(materProdEntity.getAmount());
            materialWrapList.add(wrap);
        }

        domain.setMaterialProducts(materialWrapList);

        return domain;
    }
}
