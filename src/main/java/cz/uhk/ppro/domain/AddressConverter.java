package cz.uhk.ppro.domain;

import cz.uhk.ppro.entity.AddressEntity;
import cz.uhk.ppro.util.Converter;


public class AddressConverter extends Converter<AddressEntity, Address> {

	@Override
	public AddressEntity toEntity(Address domain) {
		if (domain == null){
			return null;
		}
		AddressEntity entity = new AddressEntity();
		entity.setAdresyId(domain.getAddressId());
		entity.setMesto(domain.getCity());
		entity.setPsc(domain.getPostCode());
		entity.setUlice(domain.getStreet());

		return entity;
	}

	@Override
	public Address toDTO(AddressEntity entity) {
		if (entity == null){
			return null;
		}
		Address address = new Address();
		address.setAddressId(entity.getAdresyId());
		address.setCity(entity.getMesto());
		address.setPostCode(entity.getPsc());
		address.setStreet(entity.getUlice());

		return address;
	}

}
