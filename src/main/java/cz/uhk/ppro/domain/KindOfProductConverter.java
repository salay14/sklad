package cz.uhk.ppro.domain;

import cz.uhk.ppro.entity.KindOfProductEntity;
import cz.uhk.ppro.util.Converter;

public class KindOfProductConverter extends Converter<KindOfProductEntity, KindOfProduct> {

    @Override
    public KindOfProductEntity toEntity(KindOfProduct domain) {
        if (domain == null){
            return null;
        }

        KindOfProductEntity entity = new KindOfProductEntity();
        entity.setKindOfProductEntityId(domain.getKindOfProductId());
        entity.setUse(domain.getUse());

        return entity;
    }

    @Override
    public KindOfProduct toDTO(KindOfProductEntity entity) {
        if (entity == null){
            return null;
        }

        KindOfProduct domain = new KindOfProduct();
        domain.setKindOfProductId(entity.getKindOfProductEntityId());
        domain.setUse(entity.getUse());

        return domain;
    }
}
