package cz.uhk.ppro.domain;

import cz.uhk.ppro.entity.RawMaterialEntity;
import cz.uhk.ppro.util.Converter;

public class RawMaterialConverter extends Converter<RawMaterialEntity, RawMaterial>{

    @Override
    public RawMaterialEntity toEntity(RawMaterial domain) {
        if (domain == null){
            return null;
        }

        RawMaterialEntity entity = new RawMaterialEntity();
        entity.setRawMaterialId(domain.getRawMaterialId());
        entity.setName(domain.getName());
        entity.setAmount(domain.getAmount());
        entity.setMaterial(domain.getMaterial());
        entity.setWeight(domain.getWeight());

        return entity;
    }

    @Override
    public RawMaterial toDTO(RawMaterialEntity entity) {
        if (entity == null){
            return null;
        }

        RawMaterial domain = new RawMaterial();
        domain.setRawMaterialId(entity.getRawMaterialId());
        domain.setAmount(entity.getAmount());
        domain.setMaterial(entity.getMaterial());
        domain.setName(entity.getName());
        domain.setWeight(entity.getWeight());

        return domain;
    }
}
