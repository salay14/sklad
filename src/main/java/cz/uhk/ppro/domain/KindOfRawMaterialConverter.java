package cz.uhk.ppro.domain;

import cz.uhk.ppro.entity.KindOfRawMaterialEntity;
import cz.uhk.ppro.util.Converter;

/**
 * nehodilo se pouzivat kategorii i u suroviny
 */
@Deprecated
public class KindOfRawMaterialConverter extends Converter<KindOfRawMaterialEntity, KindOfRawMaterial> {

    @Override
    public KindOfRawMaterialEntity toEntity(KindOfRawMaterial domain) {
        if (domain == null){
            return null;
        }
        KindOfRawMaterialEntity entity = new KindOfRawMaterialEntity();
        entity.setKindOfRawMaterialId(domain.getKindOfRawMaterialId());
        entity.setName(domain.getName());
        return entity;
    }

    @Override
    public KindOfRawMaterial toDTO(KindOfRawMaterialEntity entity) {
        if (entity == null){
            return null;
        }
        KindOfRawMaterial domain = new KindOfRawMaterial();
        domain.setKindOfRawMaterialId(entity.getKindOfRawMaterialId());
        domain.setName(entity.getName());
        return domain;
    }
}
