package cz.uhk.ppro.domain;


import java.io.Serializable;
import java.util.List;

public class Product implements Serializable{

    private Integer productId;
    private Integer serialNumber;
    private Integer depth;
    private Integer width;
    private Integer height;
    private String name;
    private KindOfProduct kindOfProduct;
    private List<ProductMaterialWrap> materialProducts;


    public Product() {
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KindOfProduct getKindOfProduct() {
        return kindOfProduct;
    }

    public void setKindOfProduct(KindOfProduct kindOfProduct) {
        this.kindOfProduct = kindOfProduct;
    }

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<ProductMaterialWrap> getMaterialProducts() {
        return materialProducts;
    }

    public void setMaterialProducts(List<ProductMaterialWrap> materialProducts) {
        this.materialProducts = materialProducts;
    }

    @Override
    public String toString() {
        return serialNumber +
                " | " + name;
    }
}
