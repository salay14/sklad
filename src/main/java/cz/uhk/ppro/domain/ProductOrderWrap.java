package cz.uhk.ppro.domain;

import java.io.Serializable;

public class ProductOrderWrap implements Serializable {

    private Product product;
    private String color;
    private Integer amount;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return product.getName();
    }
}
