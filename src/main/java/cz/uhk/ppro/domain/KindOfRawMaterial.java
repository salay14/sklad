package cz.uhk.ppro.domain;

import java.io.Serializable;

/**
 * nehodilo se pouzivat kategorii i u suroviny
 */
@Deprecated
public class KindOfRawMaterial implements Serializable{

    private Integer kindOfRawMaterialId;
    private String name;

    public KindOfRawMaterial() {
        super();
    }

    public KindOfRawMaterial(Integer kindOfRawMaterialId, String name) {
        this.kindOfRawMaterialId = kindOfRawMaterialId;
        this.name = name;
    }

    public Integer getKindOfRawMaterialId() {
        return kindOfRawMaterialId;
    }

    public void setKindOfRawMaterialId(Integer kindOfRawMaterial) {
        this.kindOfRawMaterialId = kindOfRawMaterial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
