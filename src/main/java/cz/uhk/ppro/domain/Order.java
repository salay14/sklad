package cz.uhk.ppro.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Order implements Serializable {

    private Integer orderId;
    private Date deliveryDate;
    private Date orderingDate;
    private Customer customer;
    private Employee employee;
    private List<ProductOrderWrap> productOrder;

    public Order() {
        super();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getOrderingDate() {
        return orderingDate;
    }

    public void setOrderingDate(Date orderingDate) {
        this.orderingDate = orderingDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<ProductOrderWrap> getProductOrder() {
        return productOrder;
    }

    public void setProductOrder(List<ProductOrderWrap> productOrder) {
        this.productOrder = productOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        if (orderId != null ? !orderId.equals(order.orderId) : order.orderId != null) return false;
        if (deliveryDate != null ? !deliveryDate.equals(order.deliveryDate) : order.deliveryDate != null) return false;
        if (orderingDate != null ? !orderingDate.equals(order.orderingDate) : order.orderingDate != null) return false;
        if (customer != null ? !customer.equals(order.customer) : order.customer != null) return false;
        if (employee != null ? !employee.equals(order.employee) : order.employee != null) return false;
        return productOrder != null ? productOrder.equals(order.productOrder) : order.productOrder == null;
    }

    @Override
    public int hashCode() {
        int result = orderId != null ? orderId.hashCode() : 0;
        result = 31 * result + (deliveryDate != null ? deliveryDate.hashCode() : 0);
        result = 31 * result + (orderingDate != null ? orderingDate.hashCode() : 0);
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (employee != null ? employee.hashCode() : 0);
        result = 31 * result + (productOrder != null ? productOrder.hashCode() : 0);
        return result;
    }
}
