package cz.uhk.ppro.view.product;

import cz.uhk.ppro.dao.ProductDao;
import cz.uhk.ppro.domain.Product;
import cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.AbstractEditablePropertyColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.RequiredEditableTextFieldColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.EditableListDataProvider;
import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.List;

public class ProductPage extends BasePage{

	@SpringBean
	private ProductDao productDao;
	
	private FeedbackPanel feedbackPanel;
	
	public ProductPage() {
		
		feedbackPanel = new FeedbackPanel("feedBack");
		feedbackPanel.setOutputMarkupPlaceholderTag(true);
		
		add(feedbackPanel);
		
		add(new Link<String>("newProduct") {
			@Override
				public void onClick() {
			    	setResponsePage(NewProductPage.class);
			   	}	
		});
		

		add(new CustomEditableGrid<Product, String>("grid", getColumns(), new EditableListDataProvider<>(getProducts()), 10, Product.class)
		{

			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<Product> rowModel) {
				productDao.remove(rowModel.getObject());
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.delete").getString());
			}

			@Override
			protected void onEdit(AjaxRequestTarget target, IModel<Product> rowModel) {
				PageParameters parameters = new PageParameters();
				parameters.set("productId", rowModel.getObject().getProductId());
				setResponsePage(NewProductPage.class, parameters);
			}
		}.setTableCss("table table-bordered table-hover table-condensed table-seznam"));
	}

	private List<AbstractEditablePropertyColumn<Product, String>> getColumns()
	{
		List<AbstractEditablePropertyColumn<Product, String>> columns = new ArrayList<>();
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("serialNumber"), "serialNumber", false));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("name"), "name"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("kindOfProduct"), "kindOfProduct.use"));
		// zobrazí pouze jednu surovinu
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("materialProducts"), "materialProducts.0.rawMaterial.name"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("height"), "height"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("width"), "width"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("depth"), "depth"));
		
		return columns;
	}

	private List<Product> getProducts(){
		return productDao.findAll();
	}


	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("title.products"));
		return titleModel;
	}
}
