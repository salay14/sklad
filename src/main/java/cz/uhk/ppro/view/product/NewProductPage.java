package cz.uhk.ppro.view.product;

import cz.uhk.ppro.dao.ProductDao;
import cz.uhk.ppro.domain.Product;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class NewProductPage extends BasePage{

	@SpringBean
	private ProductDao productDao;
	
	public NewProductPage(PageParameters parameters) {

		if(!parameters.get("productId").isEmpty() || !parameters.get("productId").isNull()){
			add(new NewProductPanel("newProductForm", Model.of(productDao.findById(parameters.get("productId").toInteger()))));
		} else {
			add(new NewProductPanel("newProductForm", Model.of(new Product())));
		}
		
	}

	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("title.newProduct"));
		return titleModel;
	}
}
