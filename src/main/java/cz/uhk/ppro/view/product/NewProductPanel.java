package cz.uhk.ppro.view.product;

import cz.uhk.ppro.dao.ProductDao;
import cz.uhk.ppro.domain.Product;
import cz.uhk.ppro.domain.ProductMaterialWrap;
import cz.uhk.ppro.view.basecomponent.KindOfProductDropDownChoise;
import cz.uhk.ppro.view.basecomponent.RawMaterialEditableRequiredDropDownCellPanel;
import cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.AbstractEditablePropertyColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableCellPanel;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.RequiredEditableTextFieldColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.EditableListDataProvider;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.List;

public class NewProductPanel extends Panel {

	@SpringBean
	private ProductDao productDao;

	private Product product;
	
	private FeedbackPanel feedbackPanel;

	public NewProductPanel(String id, IModel<Product> model) {
		super(id, model);
		this.product = model.getObject();
		
		feedbackPanel = new FeedbackPanel("feedBack");
		feedbackPanel.setOutputMarkupPlaceholderTag(true);

		add(feedbackPanel);

        CustomEditableGrid materials = new CustomEditableGrid<ProductMaterialWrap,String>("productMaterialsGrid", getColumns(), new EditableListDataProvider<>(getData()), 10, ProductMaterialWrap.class)
        {
        	@Override
			protected void onCancel(AjaxRequestTarget target) {
				target.add(feedbackPanel);
			}

			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<ProductMaterialWrap> rowModel) {
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.delete").getString());
			}

			@Override
			protected void onSave(AjaxRequestTarget target, IModel<ProductMaterialWrap> rowModel) {
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.save").getString());
			}
        	
        	@Override
        	protected void onError(AjaxRequestTarget target) {
        		target.add(feedbackPanel);
				error(new StringResourceModel("feedback.error").getString());
        	}
        	
        	@Override
			protected void onAdd(AjaxRequestTarget target, ProductMaterialWrap newRow) {
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.add").getString());
        	}	
        }.setTableCss("table table-bordered table-hover table-condensed table-seznam product-materials-table");
		
		Form<Product> form = new Form<Product>("newProductForm"){
//			@Override
//			protected void onSubmit() {
//				model.getObject().setMaterialProducts(materials.getData());
//				if (model.getObject().getProductId() == null){
//					productDao.persist(model.getObject());
//				} else {
//					productDao.merge(model.getObject());
//				}
//			}
		};
		
		form.setDefaultModel(new CompoundPropertyModel<>(model));
		form.add(new NumberTextField<Integer>("serialNumber").setRequired(true));
		form.add(new KindOfProductDropDownChoise("kindOfProduct").setRequired(true));
		form.add(new TextField<String>("name").setRequired(true));
		form.add(new NumberTextField<Integer>("width").setRequired(true));
		form.add(new NumberTextField<Integer>("height").setRequired(true));
		form.add(new NumberTextField<Integer>("depth").setRequired(true));


		form.add(new Button("saveButton"){
            @Override
            public void onSubmit() {
                model.getObject().setMaterialProducts(materials.getData());
                if (model.getObject().getProductId() == null){
                    productDao.persist(model.getObject());
                } else {
                    productDao.merge(model.getObject());
                }
                setResponsePage(ProductPage.class);
            }
        });

		form.add(new Link<String>("cancelButton") {
        	@Override
            public void onClick() {
                    setResponsePage(ProductPage.class);
            }
        });

		form.add(materials);
		add(form);
	}
	
	private List<ProductMaterialWrap> getData() {
		if (product.getMaterialProducts() == null){
            return new ArrayList<>();
        } else {
		    return product.getMaterialProducts();
        }
    }
	
	 private List<AbstractEditablePropertyColumn<ProductMaterialWrap,String>> getColumns() {
		 List<AbstractEditablePropertyColumn<ProductMaterialWrap,String>> columns = new ArrayList<>();

	     columns.add(new AbstractEditablePropertyColumn<ProductMaterialWrap, String>(new ResourceModel("rawMaterial"), "rawMaterial")
	     {

	    	 public EditableCellPanel getEditableCellPanel(String componentId)
	         {
	             return new RawMaterialEditableRequiredDropDownCellPanel(componentId, this);
	         }

	     });

	     columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("amount"), "amount"));
	     return columns;	
	 }
	
	
}
