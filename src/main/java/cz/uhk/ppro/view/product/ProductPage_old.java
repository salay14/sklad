package cz.uhk.ppro.view.product;

import cz.uhk.ppro.dao.ProductDao;
import cz.uhk.ppro.domain.KindOfProduct;
import cz.uhk.ppro.domain.Product;
import cz.uhk.ppro.domain.ProductMaterialWrap;
import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.egrid.EditableGrid;
import org.wicketstuff.egrid.column.AbstractEditablePropertyColumn;
import org.wicketstuff.egrid.column.RequiredEditableTextFieldColumn;
import org.wicketstuff.egrid.component.EditableDataTable;
import org.wicketstuff.egrid.provider.EditableListDataProvider;

import java.util.ArrayList;
import java.util.List;

public class ProductPage_old extends BasePage {

	@SpringBean
	private ProductDao productDao;

	public ProductPage_old() {
		add(new EditableGrid<Product, String>("grid", getColumns(), new EditableListDataProvider<>(getData()), 10, Product.class)
		{
			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<Product> rowModel) {
			    productDao.remove(rowModel.getObject());
				super.onDelete(target, rowModel);
			}

			@Override
			protected void onSave(AjaxRequestTarget target, IModel<Product> rowModel) {
                productDao.merge(rowModel.getObject());
				super.onSave(target, rowModel);
			}

			@Override
			protected void onAdd(AjaxRequestTarget target, Product newRow) {
				productDao.persist(newRow);
				super.onAdd(target, newRow);
			}

			@Override
			protected EditableDataTable.RowItem<Product> newRowItem(String id, int index, IModel<Product> model) {
				return super.newRowItem(id, index, model);
			}
		}.setTableCss("table table-bordered table-hover table-condensed table-seznam"));
	}

	private List<AbstractEditablePropertyColumn<Product, String>> getColumns()
	{
		List<AbstractEditablePropertyColumn<Product, String>> columns = new ArrayList<>();
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("product.name"), "name"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("product.serialNumber"), "serialNumber"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("product.depth"), "depth"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("product.width"), "width"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("product.height"), "height"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("product.serialNumber"), "serialNumber"));

//		private KindOfProduct kindOfProduct;
//		private List<ProductMaterialWrap> materialProducts;

		return columns;
	}

	private List<Product> getData(){
		return productDao.findAll();
	}


	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("menu.productPage"));
		return titleModel;
	}
	
}
