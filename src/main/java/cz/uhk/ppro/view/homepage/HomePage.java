package cz.uhk.ppro.view.homepage;

import cz.uhk.ppro.dao.OrderDao;
import cz.uhk.ppro.domain.Order;
import cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.AbstractEditablePropertyColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.RequiredEditableTextFieldColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.EditableListDataProvider;
import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import cz.uhk.ppro.view.order.OrderPage;
import org.apache.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.List;

public class HomePage extends BasePage{

	static final Logger LOGGER = Logger.getLogger(HomePage.class);


	@SpringBean
	private OrderDao orderDao;

	public HomePage() {
		super();

		add(new Link("newOrder") {
			@Override
			public void onClick() {
				setResponsePage(OrderPage.class);
			}
			
			@Override
			protected void onConfigure() {
				super.onConfigure();
				if (AuthenticatedWebSession.get().isSignedIn()) {
					setVisible(true);
				} else {
					setVisible(false);
				}
			}
		});
		
		add(new CustomEditableGrid<Order, String>("grid", getColumns(), new EditableListDataProvider<>(getOrders()), 10, Order.class)
		{
			@Override
			protected boolean allowDelete(org.apache.wicket.markup.repeater.Item<Order> rowItem) {
				return AuthenticatedWebSession.get().isSignedIn();
			}

			@Override
            protected void onEdit(AjaxRequestTarget target, IModel<Order> rowModel) {
                PageParameters pageParameters = new PageParameters();
                pageParameters.set("orderId", rowModel.getObject().getOrderId());
                setResponsePage(OrderPage.class, pageParameters);
            }

			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<Order> rowModel) {
				orderDao.remove(rowModel.getObject());
			}

			@Override
			protected void onSave(AjaxRequestTarget target, IModel<Order> rowModel) {
				orderDao.merge(rowModel.getObject());
                LOGGER.debug("Saving: " +rowModel.getObject().toString());
			}

		}.setTableCss("table table-bordered table-hover table-condensed table-seznam"));	
		
	}
	
	
	private List<AbstractEditablePropertyColumn<Order, String>> getColumns()
	{
		List<AbstractEditablePropertyColumn<Order, String>> columns = new ArrayList<>();
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("orderId"), "orderId", false));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("customer"), "customer", false));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("employee"), "employee"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("deliveryDate"), "deliveryDate"));
		
		return columns;
	}

	private List<Order> getOrders(){
		return orderDao.findAll();
	}

	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("title.orders"));
		return titleModel;
	}
}
