package cz.uhk.ppro.view.basecomponent;

import cz.uhk.ppro.dao.KindOfRawMaterialDao;
import cz.uhk.ppro.domain.KindOfRawMaterial;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

@Deprecated
public class KindOfRawMaterialDropDownChoise extends DropDownChoice<KindOfRawMaterial>{

    @SpringBean
    private KindOfRawMaterialDao kindOfRawMaterialDao;


    public KindOfRawMaterialDropDownChoise(String id, IModel<KindOfRawMaterial> model) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("name", "kindOfRawMaterialId"));
        setModel(model);
        setChoices(kindOfRawMaterialDao.findAll());
    }

    public KindOfRawMaterialDropDownChoise(String id) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("name", "kindOfRawMaterialId"));
        setChoices(kindOfRawMaterialDao.findAll());
    }

}
