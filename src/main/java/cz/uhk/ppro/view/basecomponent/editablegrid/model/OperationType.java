package cz.uhk.ppro.view.basecomponent.editablegrid.model;

/**
 * 
 * @author Nadeem Mohammad
 * 
 */
public enum OperationType {
	DELETE, SAVE;
}
