package cz.uhk.ppro.view.basecomponent;

import cz.uhk.ppro.dao.KindOfProductDao;
import cz.uhk.ppro.dao.ProductDao;
import cz.uhk.ppro.domain.KindOfProduct;
import cz.uhk.ppro.domain.Product;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class KindOfProductDropDownChoise extends DropDownChoice<KindOfProduct>{

    @SpringBean
    private KindOfProductDao productDao;


    public KindOfProductDropDownChoise(String id, IModel<KindOfProduct> model) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("use", "kindOfProductId"));
        setModel(model);
        setChoices(productDao.findAll());
    }

    public KindOfProductDropDownChoise(String id) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("use", "kindOfProductId"));
        setChoices(productDao.findAll());
    }

}
