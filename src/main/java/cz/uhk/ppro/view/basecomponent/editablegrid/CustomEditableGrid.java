package cz.uhk.ppro.view.basecomponent.editablegrid;

import cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableGridActionsColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.component.EditableDataTable;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.EditableListDataProvider;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.IEditableDataProvider;
import cz.uhk.ppro.view.basecomponent.editablegrid.toolbar.EditableGridBottomToolbar;
import cz.uhk.ppro.view.basecomponent.editablegrid.toolbar.EditableGridHeadersToolbar;
import cz.uhk.ppro.view.basecomponent.editablegrid.toolbar.EditableGridNavigationToolbar;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IFormSubmitter;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;

import java.util.ArrayList;
import java.util.List;


public class CustomEditableGrid<T, S> extends Panel
{

	private EditableDataTable<T, S> dataTable;

	public CustomEditableGrid(final String id, final List<? extends IColumn<T, S>> columns,
							  final IEditableDataProvider<T, S> dataProvider, final long rowsPerPage, Class<T> clazz)
	{
		super(id);
		List<IColumn<T, S>> newCols = new ArrayList<IColumn<T, S>>();
		newCols.addAll(columns);
		if (allowEdit(null) || allowDelete(null)){
			newCols.add(newActionsColumn());
		}

		add(buildForm(newCols, dataProvider, rowsPerPage, clazz));
	}

	private Component buildForm(final List<? extends IColumn<T, S>> columns,
                                final IEditableDataProvider<T, S> dataProvider, long rowsPerPage, Class<T> clazz)
	{
		Form<T> form = new NonValidatingForm<T>("form");
		form.setOutputMarkupId(true);
		this.dataTable = newDataTable(columns, dataProvider, rowsPerPage, clazz);
		form.add(this.dataTable);
		return form;
	}

	public final cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid<T, S> setTableBodyCss(final String cssStyle)
	{
		this.dataTable.setTableBodyCss(cssStyle);
		return this;
	}
	
	public final cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid<T, S> setTableCss(final String cssStyle)
	{
		this.dataTable.add(AttributeModifier.replace("class", cssStyle));
		return this;
	}

	private static class NonValidatingForm<T> extends Form<T>
	{
		private static final long serialVersionUID = 1L;
		public NonValidatingForm(String id)
		{
			super(id);
		}
		@Override
		public void process(IFormSubmitter submittingComponent)
		{
			delegateSubmit(submittingComponent);
		}
		
	}

	private EditableDataTable<T, S> newDataTable(final List<? extends IColumn<T, S>> columns,
                                                 final IEditableDataProvider<T, S> dataProvider, long rowsPerPage, Class<T> clazz)
	{
		final EditableDataTable<T, S> dataTable = new EditableDataTable<T, S>("dataTable", columns,
			dataProvider, rowsPerPage, clazz)
		{

			private static final long serialVersionUID = 1L;

			@Override
			protected void onError(AjaxRequestTarget target)
			{
				cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.onError(target);
			}
			@Override
			protected Item<T> newRowItem(String id, int index, IModel<T> model) {
				return super.newRowItem(id, index, model);
			}
		};

		dataTable.setOutputMarkupId(true);

		dataTable.addTopToolbar(new EditableGridNavigationToolbar(dataTable));
		dataTable.addTopToolbar(new EditableGridHeadersToolbar<T, S>(dataTable, dataProvider));
		if (displayAddFeature())
		{			
			dataTable.addBottomToolbar(newAddBottomToolbar(dataProvider, clazz, dataTable));
		}

		return dataTable;
	}

	protected EditableDataTable.RowItem<T> newRowItem(String id, int index, IModel<T> model) {
		return new EditableDataTable.RowItem<T>(id, index, model);
	}

	private EditableGridBottomToolbar<T, S> newAddBottomToolbar(
            final IEditableDataProvider<T, S> dataProvider, Class<T> clazz,
            final EditableDataTable<T, S> dataTable)
			{
		return new EditableGridBottomToolbar<T, S>(dataTable, clazz)
				{

					private static final long serialVersionUID = 1L;

					@Override
					protected void onAdd(AjaxRequestTarget target, T newRow)
					{
						dataProvider.add(newRow);
						target.add(dataTable);
						cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.onAdd(target, newRow);
					}

					@Override
					protected void onError(AjaxRequestTarget target)
					{
						super.onError(target);
						cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.onError(target);
					}

				};
	}

	private EditableGridActionsColumn<T, S> newActionsColumn()
	{
//		return new EditableGridActionsColumn<T, S>(new Model<>("Actions"))
		return new EditableGridActionsColumn<T, S>(new ResourceModel("grid.actions"))
		{
			@Override
			protected void onError(AjaxRequestTarget target, IModel<T> rowModel)
			{
				cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.onError(target);
			}

			@Override
			protected void onSave(AjaxRequestTarget target, IModel<T> rowModel)
			{
				cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.onSave(target, rowModel);
			}

			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<T> rowModel)
			{
				cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.onDelete(target, rowModel);
			}

			@Override
			protected void onCancel(AjaxRequestTarget target)
			{
				cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.onCancel(target);
			}

            @Override
            protected void onEdit(AjaxRequestTarget target, IModel<T> rowModel) {
                cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.onEdit(target, rowModel);
            }

            @Override
			protected boolean allowDelete(Item<T> rowItem) {
				return cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.allowDelete(rowItem);
			}

			@Override
			protected boolean allowEdit(Item<T> rowItem) {
				return cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid.this.allowEdit(rowItem);
			}
		};
	}

	protected boolean allowDelete(Item<T> rowItem) {
		return true;
	}

	protected boolean allowEdit(Item<T> rowItem){
		return true;
	}

	protected void onCancel(AjaxRequestTarget target)
	{

	}


	protected void onDelete(AjaxRequestTarget target, IModel<T> rowModel)
	{

	}

	protected void onSave(AjaxRequestTarget target, IModel<T> rowModel)
	{

	}

	protected void onError(AjaxRequestTarget target)
	{

	}

	protected void onAdd(AjaxRequestTarget target, T newRow)
	{

	}

	protected void onEdit(AjaxRequestTarget target, IModel<T> rowModel){

    }
	
	protected boolean displayAddFeature() {
		return true;
	}

	public List<T> getData(){
		EditableListDataProvider editableListDataProvider = (EditableListDataProvider) dataTable.getDatagrid().getDataProvider();
		return editableListDataProvider.getData();
	}
}
