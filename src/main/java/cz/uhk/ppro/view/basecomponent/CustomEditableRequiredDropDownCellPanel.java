package cz.uhk.ppro.view.basecomponent;

import cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableCellPanel;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.IChoiceRenderer;

import java.util.List;

public class CustomEditableRequiredDropDownCellPanel<T, S> extends EditableCellPanel
{

	private static final long serialVersionUID = 1L;

	public CustomEditableRequiredDropDownCellPanel(final String id, final PropertyColumn<?, S> column, final List<? extends T> choices, IChoiceRenderer<? super T> renderer)
	{
		super(id);

        DropDownChoice<T> field = new DropDownChoice<T>("dropdown", choices, renderer);
		field.setLabel(column.getDisplayModel());
		add(field);
	}


	@Override
	public FormComponent<?> getEditableComponent()
	{
		return (FormComponent<?>) get("dropdown");
	}
}
