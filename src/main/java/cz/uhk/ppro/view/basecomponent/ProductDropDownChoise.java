package cz.uhk.ppro.view.basecomponent;

import cz.uhk.ppro.dao.ProductDao;
import cz.uhk.ppro.domain.Product;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class ProductDropDownChoise extends DropDownChoice<Product>{

    @SpringBean
    private ProductDao productDao;


    public ProductDropDownChoise(String id, IModel<Product> model) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("name", "productId"));
        setModel(model);
        setChoices(productDao.findAll());
    }

    public ProductDropDownChoise(String id) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("name", "productId"));
        setChoices(productDao.findAll());
    }

}
