package cz.uhk.ppro.view.basecomponent;

import cz.uhk.ppro.dao.CustomerDao;
import cz.uhk.ppro.domain.Customer;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class CustomerDropDownChoise extends DropDownChoice<Customer>{

    @SpringBean
    private CustomerDao customerDao;


    public CustomerDropDownChoise(String id, IModel<Customer> model) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("email", "customerId"));
        setModel(model);
        setChoices(customerDao.findAll());
    }

    public CustomerDropDownChoise(String id) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("email", "customerId"));
        setChoices(customerDao.findAll());
    }

}
