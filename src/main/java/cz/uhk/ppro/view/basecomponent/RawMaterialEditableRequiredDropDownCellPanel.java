package cz.uhk.ppro.view.basecomponent;

import cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableCellPanel;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.IChoiceRenderer;

import java.util.List;

public class RawMaterialEditableRequiredDropDownCellPanel<T, S> extends EditableCellPanel
{

	public RawMaterialEditableRequiredDropDownCellPanel(final String id, final PropertyColumn<?, S> column)
	{
		super(id);
		RawMaterialDropDownChoise dropDownChoise = new RawMaterialDropDownChoise("dropdown");
		dropDownChoise.setRequired(true);
		dropDownChoise.setLabel(column.getDisplayModel());
		add(dropDownChoise);
	}

	@Override
	public FormComponent<?> getEditableComponent()
	{
		return (FormComponent<?>) get("dropdown");
	}
}
