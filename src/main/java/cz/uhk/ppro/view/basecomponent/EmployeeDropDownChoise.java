package cz.uhk.ppro.view.basecomponent;

import cz.uhk.ppro.dao.EmployeeDao;
import cz.uhk.ppro.domain.Employee;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class EmployeeDropDownChoise extends DropDownChoice<Employee>{

    @SpringBean
    private EmployeeDao employeeDao;


    public EmployeeDropDownChoise(String id, IModel<Employee> model) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("", "employeeId"));
        setModel(model);
        setChoices(employeeDao.findAll());
    }

    public EmployeeDropDownChoise(String id) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("email", "employeeId"));
        setChoices(employeeDao.findAll());
    }

}
