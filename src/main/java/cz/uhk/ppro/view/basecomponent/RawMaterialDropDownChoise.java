package cz.uhk.ppro.view.basecomponent;

import cz.uhk.ppro.dao.RawMaterialDao;
import cz.uhk.ppro.domain.RawMaterial;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class RawMaterialDropDownChoise extends DropDownChoice<RawMaterial>{

    @SpringBean
    private RawMaterialDao rawMaterialDao;


    public RawMaterialDropDownChoise(String id, IModel<RawMaterial> model) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("name", "rawMaterialId"));
        setModel(model);
        setChoices(rawMaterialDao.findAll());
    }

    public RawMaterialDropDownChoise(String id) {
        super(id);
        setChoiceRenderer(new ChoiceRenderer<>("name", "rawMaterialId"));
        setChoices(rawMaterialDao.findAll());
    }

}
