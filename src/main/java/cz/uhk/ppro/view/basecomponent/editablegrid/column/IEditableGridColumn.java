package cz.uhk.ppro.view.basecomponent.editablegrid.column;

/**
 * 
 * @author Nadeem Mohammad
 *
 */
public interface IEditableGridColumn
{
	EditableCellPanel getEditableCellPanel(String componentId);
}
