package cz.uhk.ppro.view.basecomponent.editablegrid.column;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableGridActionsPanel;

/**
 * 
 * @author Nadeem Mohammad
 *
 */
public class EditableGridActionsColumn<T, S> extends PropertyColumn<T, S>
{

	public EditableGridActionsColumn(IModel<String> displayModel)
	{
		super(displayModel, "");		
	}

	private static final long serialVersionUID = 1L;

	@Override
	public void populateItem(final Item<ICellPopulator<T>> item, final String componentId, final IModel<T> rowModel)
	{

		item.add(new EditableGridActionsPanel<T>(componentId, item)
		{

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSave(AjaxRequestTarget target) {
				cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableGridActionsColumn.this.onSave(target, rowModel);
			}

			@Override
			protected void onError(AjaxRequestTarget target) {
				cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableGridActionsColumn.this.onError(target, rowModel);
			}

			@Override
			protected void onCancel(AjaxRequestTarget target) {
				cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableGridActionsColumn.this.onCancel(target);
			}

			@Override
			protected void onDelete(AjaxRequestTarget target) {
				cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableGridActionsColumn.this.onDelete(target, rowModel);
			}

			@Override
			protected void onEdit(AjaxRequestTarget target) {
				cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableGridActionsColumn.this.onEdit(target, rowModel);
			}

			@Override
			protected boolean allowDelete(Item<T> rowItem) {
				return cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableGridActionsColumn.this.allowDelete(rowItem);
			}

			@Override
			protected boolean allowEdit(Item<T> rowItem) {
				return cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableGridActionsColumn.this.allowEdit(rowItem);
			}
			
		});
	}
	
	protected boolean allowDelete(Item<T> rowItem) {
		return true;
	}

	protected boolean allowEdit(Item<T> rowItem) {
		return true;
	}

	protected void onDelete(AjaxRequestTarget target, IModel<T> rowModel)
	{				
		
	}

	protected void onSave(AjaxRequestTarget target, IModel<T> rowModel)
	{
				
	}

	protected void onError(AjaxRequestTarget target, IModel<T> rowModel)
	{
				
	}
	protected void onCancel(AjaxRequestTarget target) {

	}

	protected void onEdit(AjaxRequestTarget target, IModel<T> rowModel) {

	}
}
