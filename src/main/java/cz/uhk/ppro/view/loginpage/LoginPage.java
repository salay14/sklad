package cz.uhk.ppro.view.loginpage;

import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import cz.uhk.ppro.view.homepage.HomePage;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.validation.validator.StringValidator;

public class LoginPage extends BasePage {
	
	@Override
	protected void onConfigure() {
	
	}

	private static final long serialVersionUID = 1L;

	private String username;
    private String password;
    private boolean rememberMe;
    
    private FeedbackPanel feedbackPanel;

    public LoginPage() {
    	
    	feedbackPanel = new FeedbackPanel("feedBack");
		feedbackPanel.setOutputMarkupPlaceholderTag(true);
		
            setDefaultModel(new CompoundPropertyModel<LoginPage>(this));

            FeedbackPanel feedback = new FeedbackPanel("feedBack");
            add(feedback);

            Form<?> loginForm = new Form<Object>("loginForm") {
   
				private static final long serialVersionUID = 1L;

					@Override
                    protected void onSubmit() {
                            if (AuthenticatedWebSession.get().signIn(username, password)) {
                                    setResponsePage(HomePage.class);
                            } else {
                            	error(new StringResourceModel("feedback.loginError").getString());
                            }
           //                 success(new StringResourceModel("feedback.add").getString());
                    }
					
					@Override
					protected void onError() {
						// TODO Auto-generated method stub
						super.onError();
			//			error(new StringResourceModel("feedback.loginError").getString());
					}
            };
            add(loginForm);

            loginForm.add(new TextField<>("username", new PropertyModel<String>(this, "username")).setRequired(true));
            loginForm.add(new PasswordTextField("password", new PropertyModel<String>(this, "password")).setRequired(true).add(StringValidator.minimumLength(6)));
            loginForm.add(new CheckBox("rememberMe", new PropertyModel<Boolean>(this, "rememberMe")));
            
    }

    @Override
    public TitleModel getTitleModelObject() {
            titleModel.setObject(getString("title.login"));
			return titleModel;
    }
   
}
