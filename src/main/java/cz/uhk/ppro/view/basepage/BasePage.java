package cz.uhk.ppro.view.basepage;

import cz.uhk.ppro.view.customer.CustomerPage;
import cz.uhk.ppro.view.employee.EmployeePage;
import cz.uhk.ppro.view.homepage.HomePage;
import cz.uhk.ppro.view.kindofproduct.KindOfProductPage;
import cz.uhk.ppro.view.loginpage.LoginPage;
import cz.uhk.ppro.view.product.ProductPage;
import cz.uhk.ppro.view.rawmaterial.RawMaterialPage;
import org.apache.wicket.Application;
import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.ResourceModel;

import java.util.Locale;

public abstract class BasePage extends WebPage {

	protected TitleModel titleModel = new TitleModel();

	@Override
	protected void onConfigure() {
	        super.onConfigure();

	        AuthenticatedWebApplication application = (AuthenticatedWebApplication) Application.get();
	        AuthenticatedWebSession session = AuthenticatedWebSession.get();
	        if (!session.isSignedIn()) {
	        // Až bude fungovat login, tak od komentovat
//	                application.restartResponseAtSignInPage();
	        }
	}

	public BasePage() {

	add(new Link<String>("homePage") {
		@Override
			public void onClick() {
		    	setResponsePage(HomePage.class);
		   	}	
		@Override
		protected void disableLink(ComponentTag tag) {
			// TODO Auto-generated method stub
			super.disableLink(tag);
		}
	});
			 
    add(new Link<String>("employeePage") {
    	@Override
        public void onClick() {
                setResponsePage(EmployeePage.class);
        }
    	
    	@Override
		protected void onConfigure() {
			super.onConfigure();
			if (AuthenticatedWebSession.get().isSignedIn()) {
				setVisible(true);
			} else {
				setVisible(false);
			}
		}
    });
	
    add(new Link<String>("customerPage") {
    	@Override
        public void onClick() {
                setResponsePage(CustomerPage.class);
        }
    	
    	@Override
		protected void onConfigure() {
			super.onConfigure();
			if (AuthenticatedWebSession.get().isSignedIn()) {
				setVisible(true);
			} else {
				setVisible(false);
			}
		}
    });
	       
    add(new Link<String>("rawMaterialPage") {
    	@Override
        public void onClick() {
                setResponsePage(RawMaterialPage.class);
        }
    });
    add(new Link<String>("productPage") {
    	@Override
        public void onClick() {
               setResponsePage(ProductPage.class);
        }
    });
    add(new Link<String>("kindOfProductPage") {
    	@Override
        public void onClick() {
               setResponsePage(KindOfProductPage.class);
        }
    });
    add(new Link<String>("login") {
		@Override
		public void onClick() {
			AuthenticatedWebSession.get().invalidate();
			setResponsePage(LoginPage.class);
		}

		@Override
		protected void onConfigure() {
			super.onConfigure();
			if (AuthenticatedWebSession.get().isSignedIn()) {
				setVisible(false);
			} else {
				setVisible(true);
			}
		}
	});
	add(new Link<String>("logout") {
		@Override
		public void onClick() {
			AuthenticatedWebSession.get().invalidate();
			setResponsePage(HomePage.class);
		}

		@Override
		protected void onConfigure() {
			super.onConfigure();
			if (AuthenticatedWebSession.get().isSignedIn()) {
				setVisible(true);
			} else {
				setVisible(false);
			}
		}
	});
	add(new Label("welcome", new ResourceModel("welcome")) {
    	@Override
		protected void onConfigure() {
			super.onConfigure();
			if (AuthenticatedWebSession.get().isSignedIn()) {
				setVisible(true);
			} else {
				setVisible(false);
			}
		}
    });
	add(new Label("admin", new ResourceModel("admin")) {
    	@Override
		protected void onConfigure() {
			super.onConfigure();
			if (AuthenticatedWebSession.get().isSignedIn()) {
				setVisible(true);
			} else {
				setVisible(false);
			}
		}
    });
	add(new Label("line", new ResourceModel("line")) {
    	@Override
		protected void onConfigure() {
			super.onConfigure();
			if (AuthenticatedWebSession.get().isSignedIn()) {
				setVisible(true);
			} else {
				setVisible(false);
			}
		}
    });
		
		add(new Link<String>("en") {
			@Override
			public void onClick() {
				Session.get().setLocale(Locale.ENGLISH);
				titleModel = getTitleModelObject();
			}
		});

		add(new Link<String>("cs") {
			@Override
			public void onClick() {
				Session.get().setLocale(new Locale("cs"));
				titleModel = getTitleModelObject();
			}
		});

		titleModel = getTitleModelObject();
		Label titleLB = new Label("title", titleModel);
		add(titleLB);


       	}

	public abstract TitleModel getTitleModelObject();

}
