package cz.uhk.ppro.view.basepage;

import org.apache.wicket.model.IModel;

public class TitleModel implements IModel<String> {
	
	private static final long serialVersionUID = 1L;

	private String title = "";
	
	@Override
	public void detach() {
		//empty
	}

	@Override
	public String getObject() {
		return title;
	}

	@Override
	public void setObject(String object) {
		this.title = object;
	}

}
