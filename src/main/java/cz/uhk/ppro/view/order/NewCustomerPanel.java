package cz.uhk.ppro.view.order;

import cz.uhk.ppro.domain.Customer;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.form.EmailTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.validator.EmailAddressValidator;

public class NewCustomerPanel extends Panel {

    public NewCustomerPanel(String id, IModel<Customer> model) {
        super(id, model);

        setDefaultModel(new CompoundPropertyModel<>(model));
       	
       	 TextField<String> firstName = new TextField<>("firstName");
            firstName.setRequired(true);
            add(firstName);

            TextField<String> surname = new TextField<>("surname");
            surname.setRequired(true);
            add(surname);

            EmailTextField email = new EmailTextField("email");
            email.setRequired(true).add(EmailAddressValidator.getInstance());
            add(email);

            TextField<String> phoneNumber = new TextField<>("phoneNumber");
            phoneNumber.setRequired(true);
            add(phoneNumber);

            TextField<String> city = new TextField<>("address.city");
            city.setRequired(true);
            add(city);

            TextField<String> street = new TextField<>("address.street");
            street.setRequired(true);
            add(street);

            TextField<String> postCode = new TextField<>("address.postCode");
            postCode.setRequired(true);
            add(postCode);
   
    }
}
