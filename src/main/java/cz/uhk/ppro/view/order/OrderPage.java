package cz.uhk.ppro.view.order;

import cz.uhk.ppro.dao.OrderDao;
import cz.uhk.ppro.domain.Order;
import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class OrderPage extends BasePage{

	@SpringBean
	private OrderDao orderDao;

	public OrderPage(PageParameters parameters) {

        if(!parameters.get("orderId").isEmpty() || !parameters.get("orderId").isNull()){
            add(new OrderPanel("customer", Model.of(orderDao.findById(parameters.get("orderId").toInteger()))));
        } else {
            add(new OrderPanel("customer", Model.of(new Order())));
        }
	}

	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("title.newOrder"));
		return titleModel;
	}
}
