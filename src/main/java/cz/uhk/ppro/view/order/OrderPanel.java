package cz.uhk.ppro.view.order;

import com.googlecode.wicket.jquery.core.Options;
import com.googlecode.wicket.jquery.ui.form.datepicker.DatePicker;
import cz.uhk.ppro.dao.CustomerDao;
import cz.uhk.ppro.dao.OrderDao;
import cz.uhk.ppro.dao.ProductDao;
import cz.uhk.ppro.domain.Customer;
import cz.uhk.ppro.domain.Order;
import cz.uhk.ppro.domain.Product;
import cz.uhk.ppro.domain.ProductOrderWrap;
import cz.uhk.ppro.view.basecomponent.CustomerDropDownChoise;
import cz.uhk.ppro.view.basecomponent.EmployeeDropDownChoise;
import cz.uhk.ppro.view.basecomponent.ProductEditableRequiredDropDownCellPanel;
import cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.AbstractEditablePropertyColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableCellPanel;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.EditableRequiredDropDownCellPanel;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.RequiredEditableTextFieldColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.EditableListDataProvider;
import cz.uhk.ppro.view.homepage.HomePage;
import org.apache.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.*;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class OrderPanel extends Panel {

    static final Logger LOGGER = Logger.getLogger(OrderPanel.class);

    @SpringBean
    private ProductDao productDao;

    @SpringBean
    private OrderDao orderDao;

    @SpringBean
    private CustomerDao customerDao;

    private IModel<Order> orderModel;

    private FeedbackPanel feedbackPanel;

    private AjaxCheckBox newCustomerCheck;

    private CustomerDropDownChoise customerDropDownChoise;

    private NewCustomerPanel ncp;

    private DatePicker datePicker;

    public OrderPanel(String id, IModel<Order> model) {
        super(id, model);
        if ( model.getObject().getCustomer() == null){
            model.getObject().setCustomer(new Customer());
        }
        this.orderModel = model;
        
        feedbackPanel = new FeedbackPanel("feedBack");
		feedbackPanel.setOutputMarkupPlaceholderTag(true);


		add(feedbackPanel);

        Label newCustomerCheckLbl = new Label("newCustomerCheckLbl", new ResourceModel("customerCheckBox")) {

            @Override
            public boolean isVisible() {
               return orderModel.getObject().getOrderId() == null;
            }
        };
        newCustomerCheckLbl.setOutputMarkupId(true);
        newCustomerCheckLbl.setOutputMarkupPlaceholderTag(true);
        add(newCustomerCheckLbl);

        newCustomerCheck = new AjaxCheckBox("newCustomerCheck", new Model<>(Boolean.FALSE)) {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                LOGGER.info(newCustomerCheck.getValue());
                target.add(customerDropDownChoise);
                target.add(ncp);
            }

            @Override
            public boolean isVisible() {
                return orderModel.getObject().getOrderId() == null;
            }
        };

        add(newCustomerCheck);

		Form<Order> orderForm = new Form<>("form");

        customerDropDownChoise = new CustomerDropDownChoise("selectCustomers", new PropertyModel<>(model, "customer")){
            @Override
            public boolean isVisible() {
                return Boolean.valueOf(newCustomerCheck.getValue());
            }
        };
        customerDropDownChoise.setRequired(true);
        customerDropDownChoise.setOutputMarkupId(true);
        customerDropDownChoise.setOutputMarkupPlaceholderTag(true);
        orderForm.add(customerDropDownChoise);

		
        ncp = new NewCustomerPanel("customer",new PropertyModel<>(model, "customer")){
            @Override
            public boolean isVisible() {
                return !Boolean.valueOf(newCustomerCheck.getValue());
            }
        };
        ncp.setOutputMarkupId(true);
        ncp.setOutputMarkupPlaceholderTag(true);
        orderForm.add(ncp);

        datePicker = new DatePicker("deliveryDate",new PropertyModel<>(model,"deliveryDate"), "yy-mm-dd", new Options());
        orderForm.add(datePicker);

        model.getObject().setOrderingDate(new Date());

        EmployeeDropDownChoise employeeDropDownChoise = new EmployeeDropDownChoise("selectEmployees", new PropertyModel<>(model, "employee"));
        employeeDropDownChoise.setRequired(true);
        orderForm.add(employeeDropDownChoise);

        CustomEditableGrid products = new CustomEditableGrid<ProductOrderWrap, String>("productsGrid", getColumns(), new EditableListDataProvider<>(getData()), 10, ProductOrderWrap.class)
        {

    		@Override
			protected void onCancel(AjaxRequestTarget target) {
				target.add(feedbackPanel);
			}

			@Override
    		protected boolean allowEdit(org.apache.wicket.markup.repeater.Item<ProductOrderWrap> rowItem) {
    		    return AuthenticatedWebSession.get().isSignedIn();
    		}

            @Override
    		protected boolean allowDelete(org.apache.wicket.markup.repeater.Item<ProductOrderWrap> rowItem) {
                return AuthenticatedWebSession.get().isSignedIn();
    		}

            @Override
    		protected boolean displayAddFeature() {
    			return  (AuthenticatedWebSession.get().isSignedIn());
    		}
    		
			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<ProductOrderWrap> rowModel) {
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.delete").getString());
			}

			@Override
			protected void onSave(AjaxRequestTarget target, IModel<ProductOrderWrap> rowModel) {
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.save").getString());
			}
        	
        	@Override
        	protected void onError(AjaxRequestTarget target) {
        		target.add(feedbackPanel);
				error(new StringResourceModel("feedback.error").getString());
        	}
        	
        	@Override
			protected void onAdd(AjaxRequestTarget target, ProductOrderWrap newRow) {
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.add").getString());
			}
        }.setTableCss("table table-bordered table-hover table-condensed table-seznam product-materials-table");

        orderForm.add(products);

        orderForm.add(new Button("submitButton") {
            @Override
            protected void onConfigure() {
            	if (AuthenticatedWebSession.get().isSignedIn()) {
    				setVisible(true);
    			} else {
    				setVisible(false);
    			}
            }
        	@Override
            public void onSubmit() {
                model.getObject().setProductOrder(products.getData());

                if (model.getObject().getCustomer() == null) {
                    model.getObject().setCustomer(customerDropDownChoise.getModel().getObject());
                }

                if (model.getObject().getCustomer().getCustomerId() == null){
                    Customer customer = (Customer) customerDao.persist(model.getObject().getCustomer());
                    model.getObject().setCustomer(customer);
                }

                Order order = model.getObject();
                if (order.getOrderId() == null){
                    orderDao.persist(order);
                } else {
                    orderDao.merge(order);
                }

                setResponsePage(HomePage.class);
            }
        });
        
        orderForm.add(new Link<String>("cancelButton") {
        	@Override
            public void onClick() {
                    setResponsePage(HomePage.class);
            }
        });
        
        add(orderForm);

    }

    @Override
    public void renderHead(IHeaderResponse response) {
        try {
            String locale = datePicker.getLocale().toString();
            String path = "js/datepicker-" + locale + ".js";
            LOGGER.info(path);
            response.render(JavaScriptHeaderItem.forUrl(path));
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private List<ProductOrderWrap> getData(){
        List<ProductOrderWrap> data = orderModel.getObject().getProductOrder();
        if (data != null){
            return data;
        } else {
            return new ArrayList<>();
        }
    }

    private List<AbstractEditablePropertyColumn<ProductOrderWrap,String>> getColumns() {
        List<AbstractEditablePropertyColumn<ProductOrderWrap,String>> columns = new ArrayList<>();

        columns.add(new AbstractEditablePropertyColumn<ProductOrderWrap, String>(new ResourceModel("product"), "product")
        {

            public EditableCellPanel getEditableCellPanel(String componentId)
            {
                return new ProductEditableRequiredDropDownCellPanel<Product, String>(componentId,this);
            }

        });

        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("amount"), "amount"));

        columns.add(new AbstractEditablePropertyColumn<ProductOrderWrap, String>(new ResourceModel("color"), "color")
        {

            public EditableCellPanel getEditableCellPanel(String componentId)
            {
                return new EditableRequiredDropDownCellPanel<>(componentId, this, Arrays.asList("red","green","blue","yellow","pink","brown"));
            }

        });

        return columns;
    }
}
