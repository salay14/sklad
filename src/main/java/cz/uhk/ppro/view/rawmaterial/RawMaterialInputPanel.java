package cz.uhk.ppro.view.rawmaterial;

import cz.uhk.ppro.domain.RawMaterial;
import cz.uhk.ppro.view.basecomponent.KindOfRawMaterialDropDownChoise;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;


public class RawMaterialInputPanel extends Panel {



    public RawMaterialInputPanel(String id, IModel<RawMaterial> model) {
        super(id, model);

        IModel<RawMaterial> compound = new CompoundPropertyModel<>(model);

        Form<RawMaterial> form = new Form<>("form", compound);
        form.add(new TextField<String>("name"));
        form.add(new TextField<String>("material"));
        form.add(new TextField<Integer>("weight"));
        form.add(new TextField<Integer>("amount"));
        add(form);
    }
}
