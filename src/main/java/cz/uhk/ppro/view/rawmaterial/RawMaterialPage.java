package cz.uhk.ppro.view.rawmaterial;

import cz.uhk.ppro.dao.RawMaterialDao;
import cz.uhk.ppro.domain.RawMaterial;
import cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.AbstractEditablePropertyColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.RequiredEditableTextFieldColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.EditableListDataProvider;
import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.List;

public class RawMaterialPage extends BasePage{

	@SpringBean
	private RawMaterialDao rawMaterialDao;

    private FeedbackPanel feedbackPanel;

    public RawMaterialPage() {

        feedbackPanel = new FeedbackPanel("feedBack");
        feedbackPanel.setOutputMarkupPlaceholderTag(true);

        add(feedbackPanel);

        add(new CustomEditableGrid<RawMaterial, String>("grid", getColumns(), new EditableListDataProvider<>(getRawMaterials()), 10, RawMaterial.class)
		{
            @Override
            protected void onCancel(AjaxRequestTarget target) {
            	target.add(feedbackPanel);
            }

            @Override
            protected void onDelete(AjaxRequestTarget target, IModel<RawMaterial> rowModel) {
            	target.add(feedbackPanel);
                rawMaterialDao.remove(rowModel.getObject());
				success(new StringResourceModel("feedback.delete").getString());
            }

            @Override
            protected void onSave(AjaxRequestTarget target, IModel<RawMaterial> rowModel) {
            	target.add(feedbackPanel);
                rawMaterialDao.merge(rowModel.getObject());
                success(new StringResourceModel("feedback.save").getString());
            }

            @Override
            protected void onError(AjaxRequestTarget target) {
            	target.add(feedbackPanel);
				error(new StringResourceModel("feedback.error").getString());
            }

            @Override
            protected void onAdd(AjaxRequestTarget target, RawMaterial newRow) {
                rawMaterialDao.persist(newRow);
                target.add(feedbackPanel);
				success(new StringResourceModel("feedback.add").getString());
            }

        }.setTableCss("table table-bordered table-hover table-condensed table-seznam"));
	}

    private List<AbstractEditablePropertyColumn<RawMaterial, String>> getColumns()
    {
        List<AbstractEditablePropertyColumn<RawMaterial, String>> columns = new ArrayList<>();
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("name"), "name"));
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("material"), "material"));
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("amount"), "amount"));
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("weight"), "weight"));
        return columns;
    }

    private List<RawMaterial> getRawMaterials(){
	    return rawMaterialDao.findAll();
    }


	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("title.rawMaterials"));
		return titleModel;
	}
}
