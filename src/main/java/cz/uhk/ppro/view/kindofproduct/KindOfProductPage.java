package cz.uhk.ppro.view.kindofproduct;

import cz.uhk.ppro.dao.KindOfProductDao;
import cz.uhk.ppro.domain.KindOfProduct;
import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.AbstractEditablePropertyColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.RequiredEditableTextFieldColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.component.EditableDataTable;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.EditableListDataProvider;

import java.util.ArrayList;
import java.util.List;

public class KindOfProductPage extends BasePage {

	@SpringBean
	private KindOfProductDao kindOfProductDao;
	
	private FeedbackPanel feedbackPanel;

	public KindOfProductPage() {
		
		feedbackPanel = new FeedbackPanel("feedBack");
		feedbackPanel.setOutputMarkupPlaceholderTag(true);

		add(feedbackPanel);
		
		add(new CustomEditableGrid<KindOfProduct, String>("grid", getColumns(), new EditableListDataProvider<>(getKindOfProducts()), 10, KindOfProduct.class)
		{
			@Override
			protected void onCancel(AjaxRequestTarget target) {
				target.add(feedbackPanel);
			}
			
			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<KindOfProduct> rowModel) {
			    kindOfProductDao.remove(rowModel.getObject());
			    target.add(feedbackPanel);
				success(new StringResourceModel("feedback.delete").getString());
			}

			@Override
			protected void onSave(AjaxRequestTarget target, IModel<KindOfProduct> rowModel) {
                kindOfProductDao.merge(rowModel.getObject());
				super.onSave(target, rowModel);
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.save").getString());
			}
			
			@Override
			protected void onError(AjaxRequestTarget target) {
				target.add(feedbackPanel);
				error(new StringResourceModel("feedback.error").getString());
			}

			@Override
			protected void onAdd(AjaxRequestTarget target, KindOfProduct newRow) {
				kindOfProductDao.persist(newRow);
				super.onAdd(target, newRow);
			}

			@Override
			protected EditableDataTable.RowItem<KindOfProduct> newRowItem(String id, int index, IModel<KindOfProduct> model) {
				return super.newRowItem(id, index, model);
			}
		}.setTableCss("table table-bordered table-hover table-condensed table-seznam"));
	}

	private List<AbstractEditablePropertyColumn<KindOfProduct, String>> getColumns()
	{
		List<AbstractEditablePropertyColumn<KindOfProduct, String>> columns = new ArrayList<>();
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("use"), "use"));

		return columns;

	}

	private List<KindOfProduct> getKindOfProducts(){
		return kindOfProductDao.findAll();
	}


	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("title.kindsOfProducts"));
		return titleModel;
	}
	
}
