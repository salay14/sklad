package cz.uhk.ppro.view.kindofrawmaterial;

import cz.uhk.ppro.dao.KindOfRawMaterialDao;
import cz.uhk.ppro.domain.KindOfProduct;
import cz.uhk.ppro.domain.KindOfRawMaterial;
import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.egrid.EditableGrid;
import org.wicketstuff.egrid.column.AbstractEditablePropertyColumn;
import org.wicketstuff.egrid.column.RequiredEditableTextFieldColumn;
import org.wicketstuff.egrid.component.EditableDataTable;
import org.wicketstuff.egrid.provider.EditableListDataProvider;

import java.util.ArrayList;
import java.util.List;

public class KindOfRawMaterialPage extends BasePage {

	@SpringBean
	private KindOfRawMaterialDao kindOfRawMaterialDao;

	public KindOfRawMaterialPage() {
		add(new EditableGrid<KindOfRawMaterial, String>("grid", getColumns(), new EditableListDataProvider<>(getData()), 10, KindOfRawMaterial.class)
		{
			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<KindOfRawMaterial> rowModel) {
			    kindOfRawMaterialDao.remove(rowModel.getObject());
				super.onDelete(target, rowModel);
			}

			@Override
			protected void onSave(AjaxRequestTarget target, IModel<KindOfRawMaterial> rowModel) {
                kindOfRawMaterialDao.merge(rowModel.getObject());
				super.onSave(target, rowModel);
			}

			@Override
			protected void onAdd(AjaxRequestTarget target, KindOfRawMaterial newRow) {
				kindOfRawMaterialDao.persist(newRow);
				super.onAdd(target, newRow);
			}

			@Override
			protected EditableDataTable.RowItem<KindOfRawMaterial> newRowItem(String id, int index, IModel<KindOfRawMaterial> model) {
				return super.newRowItem(id, index, model);
			}
		}.setTableCss("table table-bordered table-hover table-condensed table-seznam"));
	}

	private List<AbstractEditablePropertyColumn<KindOfRawMaterial, String>> getColumns()
	{
		List<AbstractEditablePropertyColumn<KindOfRawMaterial, String>> columns = new ArrayList<>();
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("use"), "use"));

		return columns;

	}

	private List<KindOfRawMaterial> getData(){
		return kindOfRawMaterialDao.findAll();
	}


	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("menu.kindOfRawMaterialPage"));
		return titleModel;
	}
	
}
