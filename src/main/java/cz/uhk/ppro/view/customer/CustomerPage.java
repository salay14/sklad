package cz.uhk.ppro.view.customer;

import cz.uhk.ppro.dao.CustomerDao;
import cz.uhk.ppro.domain.Customer;
import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.AbstractEditablePropertyColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.RequiredEditableTextFieldColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.component.EditableDataTable;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.EditableListDataProvider;

import java.util.ArrayList;
import java.util.List;

public class CustomerPage extends BasePage{

	@SpringBean
	private CustomerDao customerDao;
	
	private FeedbackPanel feedbackPanel;
	
	public CustomerPage() {
		
		feedbackPanel = new FeedbackPanel("feedBack");
		feedbackPanel.setOutputMarkupPlaceholderTag(true);

		add(feedbackPanel);

		add(new CustomEditableGrid<Customer, String>("grid", getColumns(), new EditableListDataProvider<>(getCustomers()), 10, Customer.class)
		{
			@Override
			protected void onCancel(AjaxRequestTarget target) {
				target.add(feedbackPanel);
			}

			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<Customer> rowModel) {
				customerDao.remove(rowModel.getObject());
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.delete").getString());
			}

			@Override
			protected void onSave(AjaxRequestTarget target, IModel<Customer> rowModel) {
				customerDao.merge(rowModel.getObject());
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.save").getString());
			}

			@Override
			protected void onError(AjaxRequestTarget target) {
				target.add(feedbackPanel);
				error(new StringResourceModel("feedback.error").getString());
			}

			@Override
			protected void onAdd(AjaxRequestTarget target, Customer newRow) {
				customerDao.persist(newRow);
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.add").getString());
			}

			@Override
			protected EditableDataTable.RowItem<Customer> newRowItem(String id, int index, IModel<Customer> model) {
				return super.newRowItem(id, index, model);
			}
		}.setTableCss("table table-bordered table-hover table-condensed table-seznam"));
	}

	private List<AbstractEditablePropertyColumn<Customer, String>> getColumns()
	{
		List<AbstractEditablePropertyColumn<Customer, String>> columns = new ArrayList<>();
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("firstName"), "firstName"));
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("surname"), "surname"));
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("email"), "email"));
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("phoneNumber"), "phoneNumber"));
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("address.city"), "address.city"));
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("address.postCode"), "address.postCode"));
        columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("address.street"), "address.street"));
		return columns;
	}

	private List<Customer> getCustomers(){
		return customerDao.findAll();
	}


	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("title.customers"));
		return titleModel;
	}
}
