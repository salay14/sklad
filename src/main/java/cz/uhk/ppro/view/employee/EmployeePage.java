package cz.uhk.ppro.view.employee;

import cz.uhk.ppro.dao.EmployeeDao;
import cz.uhk.ppro.domain.Employee;
import cz.uhk.ppro.view.basecomponent.editablegrid.CustomEditableGrid;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.AbstractEditablePropertyColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.column.RequiredEditableTextFieldColumn;
import cz.uhk.ppro.view.basecomponent.editablegrid.provider.EditableListDataProvider;
import cz.uhk.ppro.view.basepage.BasePage;
import cz.uhk.ppro.view.basepage.TitleModel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.List;

public class EmployeePage extends BasePage{

	@SpringBean
	private EmployeeDao employeeDao;

	private FeedbackPanel feedbackPanel;
	
	public EmployeePage() {

		feedbackPanel = new FeedbackPanel("feedBack");
		feedbackPanel.setOutputMarkupPlaceholderTag(true);

		add(feedbackPanel);

		add(new CustomEditableGrid<Employee, String>("grid", getColumns(), new EditableListDataProvider<>(getEmployee()), 10, Employee.class)
		{
			@Override
			protected void onCancel(AjaxRequestTarget target) {
				target.add(feedbackPanel);
			}

			@Override
			protected void onDelete(AjaxRequestTarget target, IModel<Employee> rowModel) {
				employeeDao.remove(rowModel.getObject());
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.delete").getString());
			}

			@Override
			protected void onSave(AjaxRequestTarget target, IModel<Employee> rowModel) {
				employeeDao.merge(rowModel.getObject());
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.save").getString());
			}

			@Override
			protected void onError(AjaxRequestTarget target) {
				target.add(feedbackPanel);
				error(new StringResourceModel("feedback.error").getString());
			}

			@Override
			protected void onAdd(AjaxRequestTarget target, Employee newRow) {
				employeeDao.persist(newRow);
				target.add(feedbackPanel);
				success(new StringResourceModel("feedback.add").getString());
			}
		}.setTableCss("table table-bordered table-hover table-condensed table-seznam"));
	}

	private List<AbstractEditablePropertyColumn<Employee, String>> getColumns()
	{
		List<AbstractEditablePropertyColumn<Employee, String>> columns = new ArrayList<>();
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("firstName"), "firstName"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("surname"), "surname"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("email"), "email"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("phoneNumber"), "phoneNumber"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("address.city"), "address.city"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("address.postCode"), "address.postCode"));
		columns.add(new RequiredEditableTextFieldColumn<>(new ResourceModel("address.street"), "address.street"));

		return columns;

	}

	private List<Employee> getEmployee(){
		return employeeDao.findAll();
	}

	@Override
	public TitleModel getTitleModelObject() {
		titleModel.setObject(getString("title.employees"));
		return titleModel;
	}
}
