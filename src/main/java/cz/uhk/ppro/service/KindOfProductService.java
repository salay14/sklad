package cz.uhk.ppro.service;

import cz.uhk.ppro.entity.KindOfProductEntity;

public interface KindOfProductService {

    void add(KindOfProductEntity kindOfProductEntity);
}
