package cz.uhk.ppro.service;

import cz.uhk.ppro.domain.Address;
import cz.uhk.ppro.util.service.CRUDService;

public interface AddressService extends CRUDService<Address, Integer>{

}
