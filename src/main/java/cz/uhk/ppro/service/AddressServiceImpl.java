package cz.uhk.ppro.service;

import cz.uhk.ppro.dao.AddressDao;
import cz.uhk.ppro.domain.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService{

    @Autowired
    private AddressDao addressDao;

    @Override
    public List<Address> listAll() {
        return addressDao.findAll();
    }

    @Override
    public Address get(Integer key) {
        return addressDao.findById(key);
    }

    @Override
    public Address create(Address item) {
        return null;
    }

    @Override
    public Address update(Address item) {
        return null;
    }

    @Override
    public void remove(Address item) {

    }

    @Override
    public int removeById(Integer key) {
        return 0;
    }

    @Override
    public int removeByIds(List<Integer> key) {
        return 0;
    }

    @Override
    public int removeById(Integer id, Integer... otherIds) {
        return 0;
    }

    @Override
    public int removeAll() {
        return 0;
    }
}
