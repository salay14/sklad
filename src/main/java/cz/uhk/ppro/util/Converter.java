package cz.uhk.ppro.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @param <E> entity object
 * @param <D> domain object
 */
public abstract class Converter<E, D> {

	public abstract E toEntity(D domain);

	public abstract D toDTO(E entity);

	public List<D> toDTOList(Collection<E> entities){
		if (entities == null) {
			return null;
		}
		List<D> domainObjects = new ArrayList<>();
		for (E entity : entities) {
			domainObjects.add(toDTO(entity));
		}
		return domainObjects;
	}


	public List<E> toEntityList(Collection<D> domainObjects) {
		if (domainObjects == null) {
			return null;
		}
		List<E> entities = new ArrayList<E>();
		for (D valueObject : domainObjects) {
			entities.add(toEntity(valueObject));
		}
		return entities;
	}

	/**
	 * Helper method convert boolean to char
	 * @param value
	 * @return Y for true, N for false
	 */
	protected char booleanToChar(boolean value) {
		return value?'Y':'N';
	}

	/**
	 * Helper method convert char to boolean
	 * @param source
	 * @return true if char is Y, otherwise false
	 */
	protected final boolean parseBoolean(char source) {
		return source == 'Y';
	}

}
