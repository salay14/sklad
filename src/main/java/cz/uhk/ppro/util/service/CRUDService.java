package cz.uhk.ppro.util.service;

import java.io.Serializable;
import java.util.List;

public interface CRUDService<D extends Serializable, K extends Serializable>{

    /**
     * List all database records.
     * @return all database records or empty list if table is empty.
     */
    List<D> listAll();

    /**
     * Find database record by simple id.
     * @param key id
     * @return
     */
    D get(K key);

    /**
     * Create a new database record.
     * @param item domain object
     * @return
     */
    D create(D item);

    /**
     * Update current database record.
     * @param item domain object
     * @return
     */
    D update(D item);

    /**
     * Remove database record.
     * @param item domain object
     */
    void remove(D item);

    /**
     * Remove database record by element ID.
     * @param key id
     * @return number of entities affected.
     */
    int removeById(K key);

    /**
     * Remove database record by element IDs.
     * @param key  primary key
     * @return number of entities affected.
     */
    int removeByIds(List<K> key);

    /**
     * Remove database record by element ID.
     * @param id primary key
     * @param otherIds other entity primary keys
     * @return number of entities affected.
     */
    int removeById(final K id, K ... otherIds);


    /**
     * All entities from database.
     * @return number of entities affected.
     */
    int removeAll();


}
