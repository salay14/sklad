package cz.uhk.ppro.util.dao;


import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by saly on 26.5.17.
 * @author Dusan Salay
 * @param <D> object type
 * @param <K> key type (represents an unique identifier of object above)
 */
 public interface CRUDDao<D extends Serializable, K extends Serializable> {

    /**
     * Returns object by key.
     * @param id primary key
     * @return object loaded from database or null if not present.
     */
    D findById(final K id);

    /**
     * List all database records.
     * @return
     */
     List<D> findAll();


    /**
     * Create a new database record.
     * @param domainObject
     * @return id of new record
     */
    Serializable persist(final D domainObject);

    /**
     * Update current database record.
     * @param domainObject
     * @return
     */
     D merge(final D domainObject);

    /**
     * Remove database record.
     * @param domainObject
     */
     void remove(final D domainObject);

    /**
     * Remove database record by element ID.
     * @param id
     * @return number of entities affected.
     */
     int removeById(final K id);

    /**
     * Remove database record by element ID.
     * @param id primary key
     * @param otherIds other entity primary keys
     * @return number of entities affected.
     */
     int removeById(final K id, K ... otherIds);


    /**
     * Remove database record by element IDs.
     * @param ids  primary keys
     * @return number of entities affected.
     */
     int removeByIds(final Collection<K> ids);

    /**
     * All entities from database.
     * @return number of entities affected.
     */
     int removeAll();
}
