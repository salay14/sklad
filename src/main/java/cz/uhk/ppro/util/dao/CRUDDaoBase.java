package cz.uhk.ppro.util.dao;

import cz.uhk.ppro.util.Converter;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Help DAO abstract class for CRUD operations
 * @param <D> domain object type
 * @param <DK> domain object key type
 * @param <E> entity type
 * @param <EK> entity key type
 */
@Transactional
public abstract class CRUDDaoBase<D extends Serializable, DK extends Serializable, E extends Serializable, EK extends Serializable> implements CRUDDao<D, DK> {

    protected abstract Session getSession();

    public abstract Class<E> getEntityClass();

    public abstract String getEntityKeyColumnId();

    public abstract Converter<E, D> getConverter();

    protected abstract EK toPrimaryKey(DK domainKey);

    @Override
    public List<D> findAll() {
        return getConverter().toDTOList((List<E>)(getSession().createQuery("FROM " +getEntityClass().getSimpleName()).getResultList()));
    }

    @Override
    public D findById(DK id) {
        return getConverter().toDTO(getSession().get(getEntityClass(), toPrimaryKey(id)));
    }

    @Override
    public D persist(D domainObject) {
       if (domainObject != null){
           Session session = getSession();
           E entity = getConverter().toEntity(domainObject);
           session.save(entity);

           session.flush();
           session.clear();
           return getConverter().toDTO(entity);
       }
        return null;
    }

    @Override
    public D merge(D domainObject) {
        if (domainObject != null){
            Session session = getSession();

            E entity = (E)session.merge(getConverter().toEntity(domainObject));

            session.flush();
            session.clear();
            return getConverter().toDTO(entity);
        }
        return null;
    }

    @Override
    public void remove(D domainObject) {
        if (domainObject != null){
            Session session = getSession();
            session.delete(getConverter().toEntity(domainObject));
            session.flush();
            session.clear();
        }
    }

    @Override
    public int removeById(DK id) {
        Session session = getSession();
        int affected = 0;
        if (id != null) {
            affected = session.createQuery("DELETE FROM " + getEntityClass().getSimpleName()
                    + " WHERE " + getEntityKeyColumnId() + " = :key")
                    .setParameter("key", toPrimaryKey(id)).executeUpdate();
            session.flush();
            session.clear();
        }
        return affected;
    }

    @Override
    public int removeAll() {
        Session session = getSession();
        int affected = session.createQuery("DELETE FROM " + getEntityClass().getSimpleName())
        .executeUpdate();
        session.flush();
        session.clear();
        return affected;
    }

    @Override
    public int removeByIds(Collection<DK> ids) {
        int affected = 0;

        for (DK id : ids) {
            //tohle jsem delal naprosto strizlivej. MySql pry totalne nesnasi dotazy typu IN na velkych tabulkach.
            affected += removeById(id);
        }

        return affected;
    }

    @Override
    public int removeById(DK id, DK... otherIds) {
        List<DK> items = new ArrayList<>(Arrays.asList(otherIds));
        items.add(id);
        return removeByIds(items);
    }
}
