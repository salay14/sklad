package cz.uhk.ppro.util.dao;

import org.hibernate.Session;

public interface DatabaseAccess {

	Session getCurrentSession();
}
