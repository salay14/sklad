package cz.uhk.ppro.util;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;

public class BaseAuthenticationSession extends AuthenticatedWebSession{

	private static final long serialVersionUID = 1L;
	
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin123";

	public BaseAuthenticationSession(Request request) {
		super(request);
	}

	@Override
	protected boolean authenticate(String username, String password) {
       if (username.equals(USERNAME) && password.equals(PASSWORD)) {
            return true;
       } else {
            return false;
    }
	}

	@Override
	public Roles getRoles() {
		// TODO Auto-generated method stub
		return null;
	}

}
