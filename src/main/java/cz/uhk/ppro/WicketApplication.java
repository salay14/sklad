package cz.uhk.ppro;

import cz.uhk.ppro.util.BaseAuthenticationSession;
import cz.uhk.ppro.view.customer.CustomerPage;
import cz.uhk.ppro.view.employee.EmployeePage;
import cz.uhk.ppro.view.homepage.HomePage;
import cz.uhk.ppro.view.kindofproduct.KindOfProductPage;
import cz.uhk.ppro.view.loginpage.LoginPage;
import cz.uhk.ppro.view.order.OrderPage;
import cz.uhk.ppro.view.product.NewProductPage;
import cz.uhk.ppro.view.product.ProductPage;
import cz.uhk.ppro.view.rawmaterial.RawMaterialPage;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class WicketApplication extends AuthenticatedWebApplication
{
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
		super.init();
        getMarkupSettings().setDefaultMarkupEncoding("UTF-8");

        mountPage("/home", HomePage.class);//TODO edit just to root "/"
		mountPage("/rawMaterial", RawMaterialPage.class);
		mountPage("/order", OrderPage.class);
		mountPage("/product", ProductPage.class);
		mountPage("/kindOfProduct", KindOfProductPage.class);
		mountPage("/customer", CustomerPage.class);
		mountPage("/employee", EmployeePage.class);
		mountPage("/login", LoginPage.class);
		mountPage("/products/product", NewProductPage.class);


		// add your configuration here
//		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
//		context.register(SpringConfig.class);
//		context.register(HibernateConfig.class);
		context.scan("cz.uhk.ppro");
		context.refresh();

		getComponentInstantiationListeners().add(new SpringComponentInjector(this, context));


	}

	@Override
	protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
		return BaseAuthenticationSession.class;
	}

	@Override
	protected Class<? extends WebPage> getSignInPageClass() {
		return LoginPage.class;
	}
}
