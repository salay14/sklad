package cz.uhk.ppro;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("cz.uhk.ppro")
public class SpringConfig {

}
