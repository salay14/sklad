package cz.uhk.ppro.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="suroviny")
public class RawMaterialEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="surovinyID")
	private Integer rawMaterialId;
	
	@Column(name="nazev")
	private String name;
	
	@Column(name="kusy")
	private Integer amount;
	
	@Column(name="material")
	private String material;
	
	@Column(name="vaha")
	private Integer weight;
	
//	@OneToMany
//	private List<KindOfRawMaterialEntity> kindOfRawMaterialEntityId = new ArrayList<>();

	@OneToMany(mappedBy = "materialProductPk.material", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}) //cascade = CascadeType.ALL
	private Set<MaterialProductEntity> materialProducts = new HashSet<>();
	
	public RawMaterialEntity() {
		super();
	}

	/**
	 * @return the rawMaterialId
	 */
	public Integer getRawMaterialId() {
		return rawMaterialId;
	}

	/**
	 * @param rawMaterialId the rawMaterialId to set
	 */
	public void setRawMaterialId(Integer rawMaterialId) {
		this.rawMaterialId = rawMaterialId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the amount
	 */
	public Integer getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	/**
	 * @return the material
	 */
	public String getMaterial() {
		return material;
	}

	/**
	 * @param material the material to set
	 */
	public void setMaterial(String material) {
		this.material = material;
	}

	/**
	 * @return the weight
	 */
	public Integer getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(Integer weight) {
		this.weight = weight;
	}

//	/**
//	 * @return the kindOfRawMaterialEntityId
//	 */
//	public List<KindOfRawMaterialEntity> getKindOfRawMaterialEntityId() {
//		return kindOfRawMaterialEntityId;
//	}
//
//	/**
//	 * @param kindOfRawMaterialEntityId the kindOfRawMaterialEntityId to set
//	 */
//	public void setKindOfRawMaterialEntityId(List<KindOfRawMaterialEntity> kindOfRawMaterialEntityId) {
//		this.kindOfRawMaterialEntityId = kindOfRawMaterialEntityId;
//	}

	public Set<MaterialProductEntity> getMaterialProducts() {
		return materialProducts;
	}

	public void setMaterialProducts(Set<MaterialProductEntity> materialProducts) {
		this.materialProducts = materialProducts;
	}

	public void  addMaterialProduct(MaterialProductEntity materialProduct){
		this.materialProducts.add(materialProduct);
	}
}
