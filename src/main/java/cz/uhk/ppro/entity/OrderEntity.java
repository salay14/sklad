package cz.uhk.ppro.entity;

import cz.uhk.ppro.dao.CustomerDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="zakazky")
public class OrderEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="zakazkyID")
	private Integer orderId;
	
	@Column(name="datum_dodani")
	private Date deliveryDate;
	
	@Column(name="datum_objednani")
	private Date orderingDate;
	
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "zakazniciID")
	private CustomerEntity costumer;

	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "zamestnanciID")
	private EmployeeEntity employee;
	
	@OneToMany(mappedBy = "order", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})   // ten REMOVE tu byt nemusi nema na to vliv
                                                                                            // cascade = CascadeType.ALL, orphanRemoval = true
	private Set<ProductOrderEntity> productOrderEntities;

	public OrderEntity() {
		super();
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getOrderingDate() {
		return orderingDate;
	}

	public void setOrderingDate(Date orderingDate) {
		this.orderingDate = orderingDate;
	}

	public Set<ProductOrderEntity> getProductOrderEntities() {
		return productOrderEntities;
	}

	public void setProductOrderEntities(Set<ProductOrderEntity> productOrderEntities) {
		this.productOrderEntities = productOrderEntities;
	}

	public void addProductOrderEntities(ProductOrderEntity productOrder) {
		this.productOrderEntities.add(productOrder);
	}

	public CustomerEntity getCostumer() {
		return costumer;
	}

	public void setCostumer(CustomerEntity costumer) {
		this.costumer = costumer;
	}

	public EmployeeEntity getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeEntity employee) {
		this.employee = employee;
	}

}
