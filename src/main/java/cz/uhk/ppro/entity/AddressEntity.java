package cz.uhk.ppro.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="adresy")
public class AddressEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="adresyID")
	private Integer adresyId;
	
	@Column(name="mesto")
	private String mesto;

	@Column(name="PSC")
	private String psc;

	@Column(name="ulice")
	private String ulice;
	
	public AddressEntity() {
		super();
	}

	public AddressEntity(Integer adresyId, String mesto, String psc, String ulice) {
		super();
		this.adresyId = adresyId;
		this.mesto = mesto;
		this.psc = psc;
		this.ulice = ulice;
	}

	public Integer getAdresyId() {
		return adresyId;
	}

	public void setAdresyId(Integer adresyId) {
		this.adresyId = adresyId;
	}

	public String getMesto() {
		return mesto;
	}

	public void setMesto(String mesto) {
		this.mesto = mesto;
	}

	public String getPsc() {
		return psc;
	}

	public void setPsc(String psc) {
		this.psc = psc;
	}

	public String getUlice() {
		return ulice;
	}

	public void setUlice(String ulice) {
		this.ulice = ulice;
	}

	@Override
	public String toString() {
		return "AdresyEntity [adresyId=" + adresyId + ", mesto=" + mesto + ", psc=" + psc + ", ulice=" + ulice + "]";
	}

//	@Override
//	public boolean equals(Object o) {
//		if (this == o) return true;
//		if (!(o instanceof AddressEntity)) return false;
//
//		AddressEntity that = (AddressEntity) o;
//
//		if (adresyId != null ? !adresyId.equals(that.adresyId) : that.adresyId != null) return false;
//		if (mesto != null ? !mesto.equals(that.mesto) : that.mesto != null) return false;
//		if (psc != null ? !psc.equals(that.psc) : that.psc != null) return false;
//		return ulice != null ? ulice.equals(that.ulice) : that.ulice == null;
//	}
//
//	@Override
//	public int hashCode() {
//		int result = adresyId != null ? adresyId.hashCode() : 0;
//		result = 31 * result + (mesto != null ? mesto.hashCode() : 0);
//		result = 31 * result + (psc != null ? psc.hashCode() : 0);
//		result = 31 * result + (ulice != null ? ulice.hashCode() : 0);
//		return result;
//	}


//	@Override
//	public boolean equals(Object o) {
//		if (this == o) return true;
//		if (o == null || getClass() != o.getClass()) return false;
//
//		AddressEntity that = (AddressEntity) o;
//
//		if (adresyId != null ? !adresyId.equals(that.adresyId) : that.adresyId != null) return false;
//		if (mesto != null ? !mesto.equals(that.mesto) : that.mesto != null) return false;
//		if (psc != null ? !psc.equals(that.psc) : that.psc != null) return false;
//		return ulice != null ? ulice.equals(that.ulice) : that.ulice == null;
//	}
//
//	@Override
//	public int hashCode() {
//		int result = adresyId != null ? adresyId.hashCode() : 0;
//		result = 31 * result + (mesto != null ? mesto.hashCode() : 0);
//		result = 31 * result + (psc != null ? psc.hashCode() : 0);
//		result = 31 * result + (ulice != null ? ulice.hashCode() : 0);
//		return result;
//	}


}
