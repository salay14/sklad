package cz.uhk.ppro.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="druhy_vyrobku")
public class KindOfProductEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="druhy_vyrobkuID")
	private Integer  kindOfProductEntityId;
	
	@Column(name="pouziti")
	private String use;

	public KindOfProductEntity() {
	}

	public Integer getKindOfProductEntityId() {
		return kindOfProductEntityId;
	}

	public void setKindOfProductEntityId(Integer kindOfProductEntityId) {
		this.kindOfProductEntityId = kindOfProductEntityId;
	}

	public String getUse() {
		return use;
	}

	public void setUse(String use) {
		this.use = use;
	}

	
}
