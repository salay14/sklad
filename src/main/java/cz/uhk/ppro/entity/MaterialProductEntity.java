package cz.uhk.ppro.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name="surovinyvyrobku")
@AssociationOverrides({
		@AssociationOverride(name = "materialProductPk.product", joinColumns = @JoinColumn(name = "vyrobkyID")),
		@AssociationOverride(name = "materialProductPk.material", joinColumns = @JoinColumn(name = "surovinyID"))
})
public class MaterialProductEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MaterialProductPk materialProductPk;
	
	@Column(name="kusy")
	private Integer amount;

	public MaterialProductEntity() {
		super();
	}

	public MaterialProductPk getMaterialProductPk() {
		return materialProductPk;
	}

	public void setMaterialProductPk(MaterialProductPk materialProductPk) {
		this.materialProductPk = materialProductPk;
	}

	@Transient
	public RawMaterialEntity getRawMaterial(){
		return getMaterialProductPk().getMaterial();
	}

	public void setRawMaterial(RawMaterialEntity rawMaterial){
		getMaterialProductPk().setMaterial(rawMaterial);
	}

	@Transient
	public ProductEntity getProduct(){
		return getMaterialProductPk().getProduct();
	}

	public void setProduct(ProductEntity product){
		getMaterialProductPk().setProduct(product);
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}




}
