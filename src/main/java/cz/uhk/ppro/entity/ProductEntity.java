package cz.uhk.ppro.entity;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

@Entity
@Table(name="vyrobky")
public class ProductEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="vyrobkyID")
	private Integer productId;

	@Column(name="nazev")
	private String name;
	
	@Column(name="seriove_cislo")
	private Integer serialNumber;
	
	@Column(name="hloubka")
	private Integer depth;
	
	@Column(name="sirka")
	private Integer width;
	
	@Column(name="vyska")
	private Integer height;

	@OneToOne
	@JoinColumn(name = "druhy_vyrobkuID")
	private KindOfProductEntity kindOfProduct;

	@OneToMany(mappedBy = "materialProductPk.product", cascade = CascadeType.ALL)
	private Set<MaterialProductEntity> materialProducts = new HashSet<>();

	@OneToMany(mappedBy = "product")
	private Set<ProductOrderEntity> productOrderEntities = new HashSet<>();
	
	public ProductEntity() {
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getDepth() {
		return depth;
	}

	public void setDepth(Integer depth) {
		this.depth = depth;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}

	public KindOfProductEntity getKindOfProduct() {
		return kindOfProduct;
	}

	public void setKindOfProduct(KindOfProductEntity kindOfProduct) {
		this.kindOfProduct = kindOfProduct;
	}

	public Set<MaterialProductEntity> getMaterialProducts() {
		return materialProducts;
	}

	public void setMaterialProducts(Set<MaterialProductEntity> materialProducts) {
		this.materialProducts = materialProducts;
	}

	public void addMaterialProduct(MaterialProductEntity materialProduct){
		this.materialProducts.add(materialProduct);
	}

	public Set<ProductOrderEntity> getProductOrderEntities() {
		return productOrderEntities;
	}

	public void setProductOrderEntities(Set<ProductOrderEntity> productOrderEntities) {
		this.productOrderEntities = productOrderEntities;
	}

	public void addProductOrder(ProductOrderEntity productOrder) {
		this.productOrderEntities.add(productOrder);
	}
}
