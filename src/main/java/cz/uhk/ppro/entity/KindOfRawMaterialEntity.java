package cz.uhk.ppro.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="druhy_surovin")
public class KindOfRawMaterialEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="druhy_surovinID")
	private Integer kindOfRawMaterialId;
	
	@Column(name="nazev")
	private String name;
	
	

	public KindOfRawMaterialEntity() {
	}

	public Integer getKindOfRawMaterialId() {
		return kindOfRawMaterialId;
	}

	public void setKindOfRawMaterialId(Integer kindOfRawMaterialId) {
		this.kindOfRawMaterialId = kindOfRawMaterialId;
	}

	public String getName() {
		return name;
	}

	public void setName(String nazev) {
		this.name = nazev;
	}
	
}
