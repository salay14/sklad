package cz.uhk.ppro.entity;


import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class MaterialProductPk implements Serializable {

    @ManyToOne(cascade = CascadeType.ALL)
    private RawMaterialEntity material;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private ProductEntity product;

    public RawMaterialEntity getMaterial() {
        return material;
    }

    public void setMaterial(RawMaterialEntity material) {
        this.material = material;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MaterialProductPk)) return false;

        MaterialProductPk that = (MaterialProductPk) o;

        if (material != null ? !material.equals(that.material) : that.material != null) return false;
        return product != null ? product.equals(that.product) : that.product == null;
    }

    @Override
    public int hashCode() {
        int result = material != null ? material.hashCode() : 0;
        result = 31 * result + (product != null ? product.hashCode() : 0);
        return result;
    }
}
