package cz.uhk.ppro.entity;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="zakaznici")
public class CustomerEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="zakazniciID")
	private Integer zakazniciId;
	
	@Column(name="jmeno")
	private String name;
	
	@Column(name="prijmeni")
	private String surname;
	
	@Column(name="email")
	private String email;
	
	@Column(name="telefon")
	private String phoneNumber;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "adresyID")
	private AddressEntity address;

	@OneToMany(mappedBy = "costumer",
			cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	private List<OrderEntity> orders = new ArrayList<>();


	public CustomerEntity() {
	}

	public Integer getZakazniciId() {
		return zakazniciId;
	}

	public void setZakazniciId(Integer zakazniciId) {
		this.zakazniciId = zakazniciId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public List<OrderEntity> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderEntity> orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("zakazniciId", zakazniciId)
				.append("name", name)
				.append("surname", surname)
				.append("email", email)
				.append("phoneNumber", phoneNumber)
				.append("address", address)
				.append("orders", orders)
				.toString();
	}
}
