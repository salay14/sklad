package cz.uhk.ppro.entity;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name="vyrobkyzakazek")
public class ProductOrderEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vyrobkyZakazekID")
	private Integer productOrderId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "vyrobkyID")
	private ProductEntity product;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "zakazkyID")
	private OrderEntity order;
	
	@Column(name="barva")
	private String color;
	
	@Column(name="mnozstvi")
	private Integer amount;
	
	public ProductOrderEntity() {
		super();
	}

	public Integer getProductOrderId() {
		return productOrderId;
	}

	public void setProductOrderId(Integer productOrderId) {
		this.productOrderId = productOrderId;
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public OrderEntity getOrder() {
		return order;
	}

	public void setOrder(OrderEntity order) {
		this.order = order;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

}
